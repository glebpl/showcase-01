const fs = require('fs-extra');
const chalk = require('react-dev-utils/chalk');
const FileSizeReporter = require('react-dev-utils/FileSizeReporter');
const printHostingInstructions = require('react-dev-utils/printHostingInstructions');
const printBuildError = require('react-dev-utils/printBuildError');

const printFileSizesAfterBuild = FileSizeReporter.printFileSizesAfterBuild;

const paths = require('../config/paths');
const useYarn = fs.existsSync(paths.yarnLockFile);

// These sizes are pretty large. We'll warn for bundles exceeding them.
const WARN_AFTER_BUNDLE_GZIP_SIZE = 512 * 1024;
const WARN_AFTER_CHUNK_GZIP_SIZE = 1024 * 1024;

const copyPublicFolder = () => {
    fs.copySync(paths.appPublic, paths.appBuild, {
        dereference: true,
        filter: file => file !== paths.appHtml
    });
};

const logBuildWarnings = warnings => {
    console.log(chalk.yellow('Compiled with warnings.\n'));
    console.log(warnings.join('\n\n'));
    console.log(
        '\nSearch for the ' +
        chalk.underline(chalk.yellow('keywords')) +
        ' to learn more about each warning.'
    );
    console.log(
        'To ignore, add ' +
        chalk.cyan('// eslint-disable-next-line') +
        ' to the line before.\n'
    );
};

const handleBuildSuccess = ({stats, previousFileSizes, warnings}) => {
    if(warnings.length) {
        logBuildWarnings(warnings);
    } else {
        console.log(chalk.green('Compiled successfully.\n'));
    }

    console.log('File sizes after gzip:\n');

    printFileSizesAfterBuild(
        stats,
        previousFileSizes,
        paths.appBuild,
        WARN_AFTER_BUNDLE_GZIP_SIZE,
        WARN_AFTER_CHUNK_GZIP_SIZE
    );

    console.log();
};

const handleBuildError = err => {
    const tscCompileOnError = process.env.TSC_COMPILE_ON_ERROR === 'true';
    if(tscCompileOnError) {
        console.log(
            chalk.yellow(
                'Compiled with the following type errors (you may want to check these before deploying your app):\n'
            )
        );
        printBuildError(err);
    } else {
        console.log(chalk.red('Failed to compile.\n'));
        printBuildError(err);
        process.exit(1);
    }
};

module.exports = {
    copyPublicFolder
    , handleBuildError
    , handleBuildSuccess
};
