# Description
This showcase project shows basics of abstract React application: 
- I18n
- Routing
- Authorization
- Data flow using store
- React Context, HOC, Portal, function and class components  

It uses SASS as CSS processor and SVG for icons.

Some components are manually extracted from hard-weight MUI library.

Build process is based on 'create-react-app' scripts.  

# Known issues
Some files are not .tsx because this showcase is 'extracted' from real project which is migrating from JS to TS.
