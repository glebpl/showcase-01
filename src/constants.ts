export enum HttpStatus {
  NOT_FOUND = 404
  , NOT_AUTHORIZED = 401
  , FATAL_ERROR = 500
  , SERVICE_UNAVAILABLE = 503
}

export enum ErrorCode {
  FATAL = HttpStatus.FATAL_ERROR
  , NOT_AUTHORIZED = HttpStatus.NOT_AUTHORIZED
}

export enum ConnectionStatus {
  INITIAL = 0
  , OFFLINE =  5
  , ONLINE =  10
}

export enum AuthStatus {
  UNKNOWN = 0
  , NONE = 1
  , CONFIRMED = 2
}

export enum Cls {
  ACTIVE= 'active'
  , DISABLED = 'disabled'
  , FILLED = 'filled'
  , FOCUSED = 'focused'
  , HAS_ERROR = 'has-error'
  , TRANSPARENT = 'transparent'
}

/*export const CLS = {
  ACTIVE: 'active'
  , DISABLED: 'disabled'
  , FILLED: 'filled'
  , FOCUSED: 'focused'
  , HAS_ERROR: 'has-error'
  , HIDDEN: 'hidden'
  , INVISIBLE: 'invisible'
  , LOADING: 'loading'
  , OPEN: 'open'// for collapsible
  , SHOW: 'show'// used in modal
  , TRANSPARENT: 'transparent'// invisible but clickable
};*/

// Taken from material-ui
// Follow https://material.io/guidelines/motion/duration-easing.html#duration-easing-common-durations
// to learn when use what timing
export enum Duration {
  SHORTEST = 150
  //, shorter: 200
  // , short: 250
  // most basic recommended timing
  , STANDARD = 300
  // this is to be used in complex animations
  // , complex: 375
  // recommended when something is entering screen
  , ENTERING = 225
  // recommended when something is leaving screen
  , LEAVING = 195
}

