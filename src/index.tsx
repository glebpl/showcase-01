import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import './i18n/index';
import { configureStore } from './store';
import App from './components/App';
import './sass/style.sass';

ReactDOM.render(
  <Provider store={configureStore()}>
    <Router>
      <App
        location={window.location}
      />
    </Router>
  </Provider>
  , document.getElementById('root')
);
