import { TransitionProps } from "react-transition-group/Transition";
import { AnyAction, Dispatch } from 'redux';

export type BooleanMap = {[prop: string]: boolean};

export type Timeout = ReturnType<typeof window.setTimeout>;

export interface PasswordCredential {
  password: string;
  name: string;
}

export type TransitionTimeout = TransitionProps['timeout'] | 'auto';

export interface TransitionOptions {
  duration?: string | number;
  easing?: string;
  delay?: string | number;
}

export interface BasicTransitionProps {
  duration: number | string;
  delay: number | string;
}

export interface Action extends AnyAction {
  data: any;
}

export interface ThunkSimpleDispatch {
  <T extends Action>(action: T): T;
  <R>(asyncAction: ThunkSimpleAction<R>): R;
}

export type ThunkSimpleAction<R> = (dispatch: Dispatch) => R;
