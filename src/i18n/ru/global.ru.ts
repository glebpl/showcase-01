/*eslint quotes: ["warn", "double"]*/

const whichLabels = (amount: number): 0 | 1 | 2 => {
  const units = amount % 10;
  const tens = Math.floor((amount % 100) / 10);
  return (amount === 1 ? 0 : (units >= 2 && units <= 4 && tens !== 1 ? 1 :
    (units === 1 && tens !== 1 ? 0 : 2)));
};

export default {
  cancel: "Отмена"
  , error: "Ошибка"
  , language: "Язык"

  , authorization: "Вход в систему"
  , signIn: "Войти"
  , signInWithPassword: "Войти c паролем"
  , signOut: "Выйти"
  , user: "Пользователь"
  , users: "Пользователи"
  , username: "Имя пользователя"
  , enterEmail: "Введите e-mail"
  , enterPassword: "Введите пароль"
  , password: "Пароль"
  , authError: "Пользователь не найден или пароль не совпадает"
  , httpsWarning: "Внимание! Это соединение не защищено"

  , noConnection: "Нет соединения"
  , canNotConnect: "Не удаётся установить соединение"
  , connectionIsLost: "Соединение потеряно"

  , splashText: "Готовим всё необходимое"
  , pageNotFound: "Страница не найдена"
  , pageNotFoundText: "Страница была перемещена или удалена."
  , errWrongUrlParameters: "Некорректная ссылка, возможно страница была перемещена или удалена или параметры ссылки изменены вручную."
  , goToHomePage: "Перейти на главную"

  , whichLabels
};
