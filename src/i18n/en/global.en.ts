/*eslint quotes: ["warn", "double"]*/

const whichLabels = (amount: number): 0 | 1 => amount === 1 ? 0 : 1;

export default {
  ru: "Русский"
  , en: "English"
  , cancel: "Cancel"
  , error: "Error"
  , language: "Language"

  , authorization: "Authorization"
  , signIn: "Sign in"
  , signInWithPassword: "Sign in with password"
  , signOut: "Sign out"
  , user: "User"
  , users: "Users"
  , username: "Username"
  , enterEmail: "Enter your e-mail"
  , enterPassword: "Enter your password"
  , password: "Password"
  , authError: "The user not found or wrong password"
  , httpsWarning: "Attention! Connection is not secure"

  , noConnection: "No connection"
  , canNotConnect: "Can not connect with server"
  , connectionIsLost: "Connection is lost"

  , splashText: "Preparing all required"
  , pageNotFound: "Page not found"
  , pageNotFoundText: "Page was moved or deleted."
  , errWrongUrlParameters: "Incorrect link, maybe the page has been moved or deleted, or link parameters have been changed manually."
  , goToHomePage: "Go to home page"

  , whichLabels
};
