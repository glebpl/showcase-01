import i18n, { Resource, ResourceLanguage } from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';

enum NS {
  GLOBAL = 'global'
}

export enum LanguageCode {
  EN = 'en'
  , RU = 'ru'
}

// i18n languages
const ns = Object.values(NS);
const resources: Resource = {};

// require translation resources
Object.values(LanguageCode).forEach((lang: string) => {
  const langNs: ResourceLanguage = {};
  ns.forEach(key => {
    langNs[key] = require(`./${lang}/${key}.${lang}`).default;
  });
  resources[lang] = langNs;
});

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    //lng: langs[0]
    fallbackLng: LanguageCode.EN

    , detection: {
      // https://github.com/i18next/i18next-browser-languageDetector
      // exclude cookies
      order: ['querystring', 'localStorage', 'navigator', 'htmlTag']
      , lookupQuerystring: 'lang'// parameter name
      , lookupLocalStorage: 'lang'
    }
    , resources
    , ns
    , defaultNS: ns[0]
    , react: {
      wait: true
    }
  });

export default i18n;

