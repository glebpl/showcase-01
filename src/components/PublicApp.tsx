import React, { useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RouteComponentProps, StaticContext } from 'react-router';
import { Redirect, withRouter } from 'react-router-dom';
import { AuthStatus } from '../constants';
import { getAuthStatus } from '../store/selectors';
import { getSessionStorage } from '../utils/storage';
import { privatePath, URL_PRIVATE_BASE } from '../routes';
import { LayoutMain, LayoutContent } from './style/layout';
import SignIn from './sign-in/SignIn';
import PublicPromo from './PublicPromo';
import '../sass/public.scss';

const mapStateToProps = state => ({
  authorized: getAuthStatus(state) === AuthStatus.CONFIRMED
});

const storeConnector = connect(mapStateToProps);

type LocationState = {
  fromPathname?: string;
}

type PublicAppProps = RouteComponentProps<{}, StaticContext, LocationState> & ConnectedProps<typeof storeConnector>

const PublicApp: React.FC<PublicAppProps> = props => {
  useEffect(() => {
    const {state = {}} = props.location;
    if('fromPathname' in state && state.fromPathname !== URL_PRIVATE_BASE) {
      // path to redirect after authorization
      getSessionStorage().set('fromPathname', state.fromPathname);
    }
  }, []);

  return !props.authorized ? (
    <div className="layout-root layout-public">
      <LayoutMain>
        <LayoutContent>
          <PublicPromo />
          <SignIn />
        </LayoutContent>
      </LayoutMain>
    </div>
  ) : (
    <Redirect
      to={{
        pathname: privatePath(),
        state: { from: props.location }
      }}
    />
  );
};

export default storeConnector(withRouter(PublicApp));
