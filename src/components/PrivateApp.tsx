import React, { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import { Redirect, withRouter } from 'react-router-dom';
import { ConnectedProps, connect } from 'react-redux';
import { getAuthStatus } from '../store/selectors';
import { AuthStatus } from '../constants';
import { StoreState } from '../store/types';
import { URL_SIGN_IN } from '../routes';
import { getSessionStorage } from '../utils/storage';
import PrivateAppTop from './PrivateAppTop';
import { LayoutContent, LayoutMain } from './style/layout';
import SampleHeatmap from './SampleHeatmap';

const mapStateToProps = (state: StoreState) => ({
  authorized: getAuthStatus(state) === AuthStatus.CONFIRMED
});

const storeConnector = connect(mapStateToProps, null);

type PrivateAppProps = RouteComponentProps & ConnectedProps<typeof storeConnector>;

const PrivateApp: React.FC<PrivateAppProps> = props => {
  const {authorized, history, location} = props;

  useEffect(() => {
    if(authorized) {
      const storage = getSessionStorage();
      const redirectPath = getSessionStorage().get('fromPathname');
      if(redirectPath) {
        storage.remove('fromPathname');
        if(redirectPath !== location) {
          history.push(redirectPath);
        }
      }
    }
  }, [authorized]);

  return authorized ? (
    <div className="layout-root layout-private">
      <PrivateAppTop />
      <LayoutMain>
        <LayoutContent>
          <SampleHeatmap />
        </LayoutContent>
      </LayoutMain>
    </div>
  ) : (
    <Redirect
      to={{
        pathname: URL_SIGN_IN,
        state: { fromPathname: location.pathname }
      }}
    />
  );
};

export default withRouter(storeConnector(PrivateApp));
