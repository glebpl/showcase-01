import React, { forwardRef } from 'react';
import { classNames } from '../../utils/class-names';

type InputType = React.ElementType;
type ReturnType = React.ForwardRefExoticComponent<any>;

export const withClassNames = (...defaultClassNames: string[]) => (
  (Wrapped: InputType): ReturnType => ( forwardRef(
    function WithDefaultClassNames(props, ref) {
      const {className = '', ...rest} = props;
      return (
        <Wrapped
          ref={ref}
          className={classNames([...defaultClassNames, className])}
          {...rest}
        />
      );
    }
  ))
);
