import { createContext } from 'react';

export interface AppContextInterface {
  secure: boolean;
}

export default createContext<AppContextInterface>({
  secure: false
});
