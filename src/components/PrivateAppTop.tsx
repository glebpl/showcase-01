import React, { Component, useCallback, useState } from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { connect, ConnectedProps } from 'react-redux';
import { classNames } from '../utils/class-names';
import Backdrop from './style/Backdrop';
import Portal from './style/Portal';
import { IconSignOut, IconUser } from './style/icons';
import Collapse from './style/Collapse';
import { URL_SIGN_IN } from '../routes';
import { getUserName } from '../store/selectors';
import { signOut } from '../store/actions';
import { StoreState } from '../store/types';

const mapStateToProps = (state: StoreState) => ({
  userName: getUserName(state)
});

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(signOut())
});

const storeConnector = connect(mapStateToProps, mapDispatchToProps);

type PrivateAppTopProps = ConnectedProps<typeof storeConnector>

const PrivateAppTop: React.FC<PrivateAppTopProps> = props => {
  const {t} = useTranslation();
  const {signOut, userName} = props;
  const [open, setOpen] = useState(false);

  const handleClickSignOut = useCallback(e => {
    e.preventDefault();
    signOut();
  }, []);

  const handleClick = useCallback(() => {
    setOpen(!open);
  }, [open]);

  const handleClose = useCallback(() => {
    setOpen(false);
  }, []);

  return (
    <div className="layout-top">
      <div className={classNames({
        'meta-info-wrap': true
        , 'open': open
      })}>
        <div className="meta-info" onClick={handleClick}>
          <div className="meta-user">
            <IconUser />
            <span className="meta-username">
              {userName}
            </span>
          </div>
        </div>
        <Collapse in={open}>
          <div className="meta-info-ext">
            <nav>
              <div className="nav-item">
                <Link
                  to={URL_SIGN_IN}
                  className="nav-link"
                  onClick={handleClickSignOut}
                >
                  <IconSignOut /> {t('signOut')}
                </Link>
              </div>
            </nav>
          </div>
        </Collapse>
        {open ? (
          <Portal container={document.querySelector('.layout-private')}>
            <Backdrop
              open={open}
              onClick={handleClose}
              className="meta-backdrop"
            />
          </Portal>
        ) : null}
      </div>
    </div>
  );
};

export default storeConnector(PrivateAppTop);
