import React, { Component, createRef } from 'react';
import Transition from 'react-transition-group/Transition';
import { Duration } from '../../constants';
import { classNames } from '../../utils/class-names';
import { domGetAutoHeightDuration } from '../../utils/dom';
import { domGetTransitionProps } from '../../utils/dom';
import { TransitionComponentProps } from './transitions/types';
import { Timeout } from '../../types';

export interface CollapseProps extends TransitionComponentProps {
  /**
   * The height of the container when collapsed.
   */
  collapsedHeight?: string;
}

/**
 * Taken and transformed from https://material-ui.com
 * It uses [react-transition-group](https://github.com/reactjs/react-transition-group) internally.
 */
export default class Collapse extends Component<CollapseProps> {
  private autoTransitionDuration: number;
  private timer: Timeout;
  private wrapperRef: React.RefObject<HTMLDivElement> = createRef();

  static defaultProps = {
    collapsedHeight: '0px'
    , component: 'div'
    , timeout: Duration.STANDARD
  };

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  _getWrapperHeight() {
    if(this.wrapperRef.current) {
      return this.wrapperRef.current.clientHeight || 0;
    }
    return 0;
  }

  _handleEnter = el => {
    el.style.height = this.props.collapsedHeight;

    if (this.props.onEnter) {
      this.props.onEnter(el);
    }
  };

  _handleEntering = el => {
    const {timeout} = this.props;
    const wrapperHeight = this._getWrapperHeight();

    const { duration: transitionDuration } = domGetTransitionProps(this.props, {
      mode: 'enter',
    });

    if (timeout === 'auto') {
      const duration2 = domGetAutoHeightDuration(wrapperHeight);
      el.style.transitionDuration = `${duration2}ms`;
      this.autoTransitionDuration = duration2;
    } else {
      el.style.transitionDuration =
                typeof transitionDuration === 'string' ? transitionDuration : `${transitionDuration}ms`;
    }

    el.style.height = `${wrapperHeight}px`;

    if (this.props.onEntering) {
      this.props.onEntering(el);
    }
  };

  _handleEntered = node => {
    node.style.height = 'auto';

    if (this.props.onEntered) {
      this.props.onEntered(node);
    }
  };

  _handleExit = el => {
    const wrapperHeight = this._getWrapperHeight();
    el.style.height = `${wrapperHeight}px`;

    if (this.props.onExit) {
      this.props.onExit(el);
    }
  };

  _handleExiting = el => {
    const {timeout} = this.props;
    const wrapperHeight = this._getWrapperHeight();

    const {duration: transitionDuration} = domGetTransitionProps(this.props, {
      mode: 'exit',
    });

    if (timeout === 'auto') {
      const duration2 = domGetAutoHeightDuration(wrapperHeight);
      el.style.transitionDuration = `${duration2}ms`;
      this.autoTransitionDuration = duration2;
    } else {
      el.style.transitionDuration =
                typeof transitionDuration === 'string' ? transitionDuration : `${transitionDuration}ms`;
    }

    el.style.height = this.props.collapsedHeight;

    if (this.props.onExiting) {
      this.props.onExiting(el);
    }
  };

  addEndListener = (_, next) => {
    if (this.props.timeout === 'auto') {
      this.timer = setTimeout(next, this.autoTransitionDuration || 0);
    }
  };

  render() {
    const {
      children,
      // classes,
      className = '',
      collapsedHeight,
      component: Component,
      onEnter,
      onEntered,
      onEntering,
      onExit,
      onExiting,
      style,
      theme,
      timeout,
      ...other
    } = this.props;

    return (
      <Transition
        onEnter={this._handleEnter}
        onEntered={this._handleEntered}
        onEntering={this._handleEntering}
        onExit={this._handleExit}
        onExiting={this._handleExiting}
        addEndListener={this.addEndListener}
        timeout={timeout === 'auto' ? null : timeout}
        {...other}
      >
        {(state, childProps) => {
          return (
            <div
              className={classNames({
                'collapse': true
                , [className]: !!className
                , 'in': state === 'entered'
              })}
              style={{
                ...style,
                minHeight: collapsedHeight,
              }}
              {...childProps}
            >
              <div
                className="collapse-inner"
                ref={this.wrapperRef}
              >
                <div className="collapse-content">
                  {children}
                </div>
              </div>
            </div>
          );
        }}
      </Transition>
    );
  }
}


