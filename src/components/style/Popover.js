import React, { Component, createRef } from 'react';
import PropTypes from 'prop-types';
import EventListener from 'react-event-listener';
import { debounce, result, createChainedFunction } from '../../utils/functions';
import { classNames } from '../../utils/class-names';
import { domOwnerDocument, domOwnerWindow, getRect } from '../../utils/dom';
import Modal from './Modal';
import Grow from './transitions/Grow';

const popoverOriginType = PropTypes.shape({
  horizontal: PropTypes.oneOfType([
    PropTypes.number
    , PropTypes.oneOf(['left', 'center', 'right'])
  ]).isRequired
  , vertical: PropTypes.oneOfType([
    PropTypes.number
    , PropTypes.oneOf(['top', 'center', 'bottom'])
  ]).isRequired
});

const getOffsetTop = (rect, vertical) => {
  let offset = 0;

  if (typeof vertical === 'number') {
    offset = vertical;
  } else if (vertical === 'center') {
    offset = rect.height / 2;
  } else if (vertical === 'bottom') {
    offset = rect.height;
  }

  return offset;
};

const getOffsetLeft = (rect, horizontal) => {
  let offset = 0;

  if (typeof horizontal === 'number') {
    offset = horizontal;
  } else if (horizontal === 'center') {
    offset = rect.width / 2;
  } else if (horizontal === 'right') {
    offset = rect.width;
  }

  return offset;
};

const getTransformOriginValue = transformOrigin => (
  [transformOrigin.horizontal, transformOrigin.vertical].map(n => typeof n === 'number' ? `${n}px` : n).join(' ')
);

// Sum the scrollTop between two elements.
const getScrollParent = (parent, child) => {
  let element = child;
  let scrollTop = 0;

  while (element && element !== parent) {
    element = element.parentNode;
    scrollTop += element.scrollTop;
  }
  return scrollTop;
};

const getAnchorEl = anchorEl => result(anchorEl);

/**
 * Taken from https://material-ui.com/components/popover/
 */
export default class Popover extends Component {

  static defaultProps = {
    anchorReference: 'anchorEl'
    , anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'left'
    }
    , marginThreshold: 16
    , transformOrigin: {
      vertical: 'top',
      horizontal: 'left'
    }
    , transitionDuration: 'auto'
    , open: false
  };

  static propTypes = {
    /**
         * This is callback property. It's called by the component on mount.
         * This is useful when you want to trigger an action programmatically.
         * It currently only supports updatePosition() action.
         *
         * @param {object} actions This object contains all posible actions
         * that can be triggered programmatically.
         */
    action: PropTypes.func
    /**
         * This is the DOM element, or a function that returns the DOM element,
         * that may be used to set the position of the popover.
         */
    , anchorEl: PropTypes.oneOfType([PropTypes.object, PropTypes.func])
    /**
         * This is the point on the anchor where the popover's
         * `anchorEl` will attach to. This is not used when the
         * anchorReference is 'anchorPosition'.
         *
         * Options:
         * vertical: [top, center, bottom];
         * horizontal: [left, center, right].
         */
    , anchorOrigin: PropTypes.shape({
      horizontal: PropTypes.oneOfType([
        PropTypes.number
        , PropTypes.oneOf(['left', 'center', 'right'])
      ]).isRequired
      , vertical: PropTypes.oneOfType([
        PropTypes.number
        , PropTypes.oneOf(['top', 'center', 'bottom'])
      ]).isRequired
    })
    /**
         * This is the position that may be used
         * to set the position of the popover.
         * The coordinates are relative to
         * the application's client area.
         */
    , anchorPosition: PropTypes.shape({
      left: PropTypes.number.isRequired
      , top: PropTypes.number.isRequired
    })

    /*
         * This determines which anchor prop to refer to to set
         * the position of the popover.
         */
    , anchorReference: PropTypes.oneOf(['anchorEl', 'anchorPosition', 'none'])

    /**
         * The content of the component.
         */
    , children: PropTypes.node
    , className: PropTypes.string

    /**
         * A node, component instance, or function that returns either.
         * The `container` will passed to the Modal component.
         * By default, it uses the body of the anchorEl's top-level document object,
         * so it's simply `document.body` most of the time.
         */
    , container: PropTypes.oneOfType([PropTypes.object, PropTypes.func])

    /**
         * This function is called in order to retrieve the content anchor element.
         * It's the opposite of the `anchorEl` property.
         * The content anchor element should be an element inside the popover.
         * It's used to correctly scroll and set the position of the popover.
         * The positioning strategy tries to make the content anchor element just above the
         * anchor element.
         */
    , getContentAnchorEl: PropTypes.oneOfType([PropTypes.func, PropTypes.bool])

    /**
         * Specifies how close to the edge of the window the popover can appear.
         */
    , marginThreshold: PropTypes.number

    /**
         * Callback fired when the component requests to be closed.
         *
         * @param {object} event The event source of the callback.
         * @param {string} reason Can be:`"escapeKeyDown"`, `"backdropClick"`
         */
    , onClose: PropTypes.func
    /**
         * Callback fired before the component is entering.
         */
    , onEnter: PropTypes.func
    /**
         * Callback fired when the component has entered.
         */
    , onEntered: PropTypes.func
    /**
         * Callback fired when the component is entering.
         */
    , onEntering: PropTypes.func
    /**
         * Callback fired before the component is exiting.
         */
    , onExit: PropTypes.func
    /**
         * Callback fired when the component has exited.
         */
    , onExited: PropTypes.func
    /**
         * Callback fired when the component is exiting.
         */
    , onExiting: PropTypes.func
    /**
         * If `true`, the popover is visible.
         */
    , open: PropTypes.bool.isRequired
    /**
         * Properties applied to the [`Paper`](/api/paper/) element.
         */
    , PaperProps: PropTypes.object

    /**
         * This is the point on the popover which
         * will attach to the anchor's origin.
         *
         * Options:
         * vertical: [top, center, bottom, x(px)];
         * horizontal: [left, center, right, x(px)].
         */
    , transformOrigin: popoverOriginType,

    /**
         * Set to 'auto' to automatically calculate transition time based on height.
         */
    transitionDuration: PropTypes.oneOfType([
      PropTypes.number
      , PropTypes.shape({ enter: PropTypes.number, exit: PropTypes.number })
      , PropTypes.oneOf(['auto'])
    ])

    /**
         * Properties applied to the `Transition` element.
         */
    , TransitionProps: PropTypes.object
  };

  constructor() {
    super();

    if (typeof window !== 'undefined') {
      this._handleResize = debounce(() => {
        // Because we debounce the event, the open property might no longer be true
        // when the callback resolves.
        if (!this.props.open) {
          return;
        }

        this._setPositioningStyles(this.el);
      });
    }

    this._ref = createRef();
  }

  get el() {
    return this._ref.current;
  }

  componentDidMount() {
    if (this.props.action) {
      this.props.action({
        updatePosition: this._handleResize,
      });
    }
  }

  componentWillUnmount() {
    this._handleResize.clear();
  }

  _setPositioningStyles = element => {
    const positioning = this._getPositioningStyle(element);
    if (positioning.top !== null) {
      element.style.top = positioning.top;
    }
    if (positioning.left !== null) {
      element.style.left = positioning.left;
    }
    element.style.transformOrigin = positioning.transformOrigin;
  };

  _getPositioningStyle = element => {
    const { anchorEl, anchorReference, marginThreshold } = this.props;

    // Check if the parent has requested anchoring on an inner content node
    const contentAnchorOffset = this._getContentAnchorOffset(element);
    const elemRect = {
      width: element.offsetWidth,
      height: element.offsetHeight,
    };

    // Get the transform origin point on the element itself
    const transformOrigin = this._getTransformOrigin(elemRect, contentAnchorOffset);

    if (anchorReference === 'none') {
      return {
        top: null,
        left: null,
        transformOrigin: getTransformOriginValue(transformOrigin),
      };
    }

    // Get the offset of of the anchoring element
    const anchorOffset = this._getAnchorOffset(contentAnchorOffset);

    // Calculate element positioning
    let top = anchorOffset.top - transformOrigin.vertical;
    let left = anchorOffset.left - transformOrigin.horizontal;
    const bottom = top + elemRect.height;
    const right = left + elemRect.width;

    // Use the parent window of the anchorEl if provided
    const containerWindow = domOwnerWindow(getAnchorEl(anchorEl));

    // Window thresholds taking required margin into account
    const heightThreshold = containerWindow.innerHeight - marginThreshold;
    const widthThreshold = containerWindow.innerWidth - marginThreshold;

    // Check if the vertical axis needs shifting
    if (top < marginThreshold) {
      const diff = top - marginThreshold;
      top -= diff;
      transformOrigin.vertical += diff;
    } else if (bottom > heightThreshold) {
      const diff = bottom - heightThreshold;
      top -= diff;
      transformOrigin.vertical += diff;
    }

    /*warning(
            elemRect.height < heightThreshold || !elemRect.height || !heightThreshold,
            [
                'Material-UI: the popover component is too tall.',
                `Some part of it can not be seen on the screen (${elemRect.height - heightThreshold}px).`,
                'Please consider adding a `max-height` to improve the user-experience.',
            ].join('\n'),
        );*/

    // Check if the horizontal axis needs shifting
    if (left < marginThreshold) {
      const diff = left - marginThreshold;
      left -= diff;
      transformOrigin.horizontal += diff;
    } else if (right > widthThreshold) {
      const diff = right - widthThreshold;
      left -= diff;
      transformOrigin.horizontal += diff;
    }

    return {
      top: `${top}px`,
      left: `${left}px`,
      transformOrigin: getTransformOriginValue(transformOrigin),
    };
  };

  // Returns the top/left offset of the position
  // to attach to on the anchor element (or body if none is provided)
  _getAnchorOffset(contentAnchorOffset) {
    const { anchorEl, anchorOrigin, anchorReference, anchorPosition } = this.props;

    if (anchorReference === 'anchorPosition') {
      /*warning(
                anchorPosition,
                'Material-UI: you need to provide a `anchorPosition` property when using ' +
                '<Popover anchorReference="anchorPosition" />.',
            );*/
      return anchorPosition;
    }

    // If an anchor element wasn't provided, just use the parent body element of this Popover
    const anchorElement = getAnchorEl(anchorEl) || domOwnerDocument(this.el).body;
    const anchorRect = getRect(anchorElement);
    const anchorVertical = contentAnchorOffset === 0 ? anchorOrigin.vertical : 'center';

    return {
      top: anchorRect.top + getOffsetTop(anchorRect, anchorVertical),
      left: anchorRect.left + getOffsetLeft(anchorRect, anchorOrigin.horizontal),
    };
  }

  // Returns the vertical offset of inner content to anchor the transform on if provided
  _getContentAnchorOffset(element) {
    const { getContentAnchorEl, anchorReference } = this.props;
    let contentAnchorOffset = 0;

    if (getContentAnchorEl && anchorReference === 'anchorEl') {
      const contentAnchorEl = getContentAnchorEl(element);

      if (contentAnchorEl && element.contains(contentAnchorEl)) {
        const scrollTop = getScrollParent(element, contentAnchorEl);
        contentAnchorOffset =
                    contentAnchorEl.offsetTop + contentAnchorEl.clientHeight / 2 - scrollTop || 0;
      }

    }

    return contentAnchorOffset;
  }

  // Return the base transform origin using the element
  // and taking the content anchor offset into account if in use
  _getTransformOrigin(elemRect, contentAnchorOffset = 0) {
    const { transformOrigin } = this.props;
    return {
      vertical: getOffsetTop(elemRect, transformOrigin.vertical) + contentAnchorOffset,
      horizontal: getOffsetLeft(elemRect, transformOrigin.horizontal),
    };
  }

  _handleEntering = element => {
    if (this.props.onEntering) {
      this.props.onEntering(element);
    }

    this._setPositioningStyles(element);
  };

  render() {
    /* eslint-disable no-unused-vars */
    const {
      action,
      anchorEl,
      anchorOrigin,
      anchorPosition,
      anchorReference,
      children,
      className = '',
      container: containerProp,
      getContentAnchorEl,
      marginThreshold,
      // ModalClasses,
      onEnter,
      onEntered,
      onEntering,
      onExit,
      onExited,
      onExiting,
      open,
      PaperProps,
      role,
      transformOrigin,
      // TransitionComponent,
      transitionDuration: transitionDurationProp,
      TransitionProps = {},
      ...other
    } = this.props;

    let transitionDuration = transitionDurationProp;

    /* excluded if (transitionDurationProp === 'auto' && !TransitionComponent.muiSupportAuto) {
          transitionDuration = undefined;
        }*/

    // If the container prop is provided, use that
    // If the anchorEl prop is provided, use its parent body element as the container
    // If neither are provided let the Modal take care of choosing the container
    const container =
            containerProp || (anchorEl ? domOwnerDocument(getAnchorEl(anchorEl)).body : undefined);

    return (
      <Modal
        container={container}
        open={open}
        BackdropProps={{ invisible: true }}
        {...other}
      >
        <Grow
          appear
          in={open}
          onEnter={onEnter}
          onEntered={onEntered}
          onExit={onExit}
          onExited={onExited}
          onExiting={onExiting}
          role={role}
          timeout={transitionDuration}
          {...TransitionProps}
          onEntering={createChainedFunction(this._handleEntering, TransitionProps.onEntering)}
        >
          <div
            className={classNames(['popover', className])}
            ref={this._ref}
            {...PaperProps}
          >
            <EventListener target="window" onResize={this._handleResize} />
            {children}
          </div>
        </Grow>
      </Modal>
    );
  }
}

