import React, { Component, Fragment } from 'react';
import DropdownToggle, { DropdownToggleProps } from './DropdownToggle';
import DropdownMenu from './DropdownMenu';
import DropdownItemLink from './DropdownItemLink';
import { domGetData, likeEvent } from '../../../utils/dom';
import { classNames } from '../../../utils/class-names';

export enum TransformOriginHorizontal {
  Left = 'left'
  , Center = 'center'
  , Right = 'right'
}

export enum TransformOriginVertical {
  Top = 'top'
  , Center = 'center'
  , Bottom = 'bottom'
}

interface CSSTransformOrigin {
  vertical: number | TransformOriginVertical;
  horizontal: number | TransformOriginHorizontal;
}

interface DropdownMenuItemData {
  value: any
}

export interface DropdownProps {
  anchorEl?: React.ReactNode | null;
  anchorOrigin?: CSSTransformOrigin | null;
  className?: string;
  disabled?: boolean;
  formatValue?(value: string): string;
  getContentAnchorEl?: boolean | Function;
  menuClassName?: string;
  menuItems: DropdownMenuItemData[]
  onChange?: Function;
  onClose?: Function;
  onOpen?: Function;
  name?: string;
  toggleComponent?: React.ElementType;
  toggleProps?: DropdownToggleProps
  toggleRef?: React.RefObject<HTMLElement>
  value: any
}

interface DropdownState {
  anchorEl: React.ReactNode | null;
}

class Dropdown extends Component<DropdownProps, DropdownState> {
  state = {
    anchorEl: null
  };

  _handleClick = event => {
    const el = event.currentTarget;
    if(this.props.anchorEl !== null) {
      this.setState({anchorEl: el});
    }
    if(this.props.onOpen) {
      this.props.onOpen(el);
    }
  };

  _handleClose = () => {
    if(!this.props.anchorEl) {
      this.setState({anchorEl: null});
    }
    if(this.props.onClose) {
      this.props.onClose();
    }
  };

  _handleChange = e => {
    this._handleClose();
    this.props.onChange(likeEvent(
      this.props.name
      , domGetData(e.target, 'value')
      , e)
    );
  };

  render() {
    const {
      anchorEl: propAnchorEl = null
      , formatValue = v => v
      , menuItems
      , toggleComponent: Toggle = DropdownToggle
      , toggleProps = {}
      , toggleRef = null
      , value
      , disabled = false
      , menuClassName = ''
      , ...rest
    } = this.props;

    // propAnchorEl may be bool to separate from 'not provided' value
    let anchorEl = propAnchorEl === null ? this.state.anchorEl : ( propAnchorEl || null );

    const menuClassNames = classNames(['dropdown-select-menu', menuClassName]);

    return (
      <Fragment>
        <Toggle
          ref={toggleRef}
          onClick={this._handleClick}
          disabled={disabled}
          {...toggleProps}
        >
          {formatValue(value)}
        </Toggle>
        <DropdownMenu
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          open={Boolean(anchorEl)}
          onClose={this._handleClose}
          disableAutoFocusItem={false}
          className={menuClassNames}
          {...rest}
        >
          {menuItems.map(item => (
            <DropdownItemLink
              key={item.value}
              selected={item.value === value}
              data-value={item.value}
              onClick={this._handleChange}
            >
              {formatValue(item.value)}
            </DropdownItemLink>
          ))}
        </DropdownMenu>
      </Fragment>
    );
  }
}

export default Dropdown;
