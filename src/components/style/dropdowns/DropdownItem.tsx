import React, { forwardRef, HTMLAttributes } from 'react';
import { classNames } from '../../../utils/class-names';
import { Cls } from '../../../constants';

export interface DropdownItemProps extends HTMLAttributes<HTMLDivElement> {
  component?: React.ElementType;
  selected?: boolean;
}

const DropdownItem = forwardRef(function DropdownItem(props: DropdownItemProps, ref) {
  const {
    component: Component = 'div'
    , selected
    , className
    , ...rest
  } = props;

  const classes = classNames({
    'dropdown-item': true
    , [Cls.ACTIVE]: selected
    , [className]: !!className
  });

  return (
    <Component ref={ref} {...rest} className={classes} />
  );
});

export default DropdownItem;
