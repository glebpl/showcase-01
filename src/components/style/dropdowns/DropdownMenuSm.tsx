import React from 'react';
import {withClassNames} from '../../hoc/class-names';
import DropdownMenu from './DropdownMenu';

const DropdownMenuSm = withClassNames('dropdown-sm')(DropdownMenu);

export default DropdownMenuSm;
