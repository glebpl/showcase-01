import React from 'react';
import { withClassNames } from '../../hoc/class-names';
import { IconCaret } from '../icons';

export default withClassNames('fl-caret')(IconCaret);
