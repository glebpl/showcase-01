import React, { forwardRef } from 'react';
import { Link, LinkProps } from 'react-router-dom';
import { classNames } from '../../../utils/class-names';
import DropdownItem, { DropdownItemProps } from './DropdownItem';

export interface DropdownItemLinkProps extends DropdownItemProps {
  to?: LinkProps['to'];
}

const DropdownItemLink = (props: DropdownItemLinkProps, ref) => {
  const {
    className
    , ...rest
  } = props;

  return (
    <DropdownItem
      component={'to' in rest ? Link : 'a'}
      className={classNames(['dropdown-item-link', className])}
      ref={ref}
      {...rest}
    />
  );
};

export default forwardRef(DropdownItemLink);
