import React, { forwardRef, HTMLAttributes } from 'react';
import { classNames } from '../../../utils/class-names';
import { Cls } from '../../../constants';

export interface DropdownToggleProps extends HTMLAttributes<HTMLDivElement> {
  disabled?: boolean
}

const DropdownToggle: React.FC<DropdownToggleProps> = ({className, disabled, ...rest}, ref) => {
    return (
      <div
        {...rest}
        ref={ref}
        className={classNames({
          'dropdown-toggle': true
          , [Cls.DISABLED]: disabled
          , className: !!className
        })}
      />
    );
};

export default forwardRef(DropdownToggle);
