import React from 'react';

const DropdownItemSeparator = () => {
  return (
    <div className="dropdown-item-separator" />
  );
};

export default DropdownItemSeparator;
