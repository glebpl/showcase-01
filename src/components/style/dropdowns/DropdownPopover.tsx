import React from 'react';
import PropTypes from 'prop-types';
import Popover from '../Popover';
import { classNames } from '../../../utils/class-names';

const popoverOriginType = PropTypes.shape({
  horizontal: PropTypes.oneOfType([
    PropTypes.number
    , PropTypes.oneOf(['left', 'center', 'right'])
  ]).isRequired
  , vertical: PropTypes.oneOfType([
    PropTypes.number
    , PropTypes.oneOf(['top', 'center', 'bottom'])
  ]).isRequired
});

const LTR_ORIGIN = {
  vertical: 'top'
  , horizontal: 'left'
};

const DropdownPopover = ({className, ...rest}) => {
  return (
    <Popover
      anchorOrigin={LTR_ORIGIN}
      transformOrigin={LTR_ORIGIN}
      className={classNames(['dropdown', className])}
      {...rest}
    />
  );
};

DropdownPopover.propTypes = {
  anchorOrigin: popoverOriginType
  , transformOrigin: popoverOriginType
};

export default DropdownPopover;
