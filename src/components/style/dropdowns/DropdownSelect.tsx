import React from 'react';
import { classNames } from '../../../utils/class-names';
import DropdownSelectInput from './DropdownSelectInput';
import FlGroup from '../FlGroup';
import { Cls } from '../../../constants';

const DropdownSelect = props => {
  const {
    className = ''
    , label = ''
    , ...rest
  } = props;

  return (
    <FlGroup
      className={classNames(['fl-group-select', Cls.FILLED, className])}
    >
      {label ? (
        <label className="fl-label">
          {label}
        </label>
      ) : null}
      <div className="fl-control">
        <DropdownSelectInput
          selectDisplayProps={{
            className: 'fl-like-field'
          }}
          {...rest}
        />
      </div>
    </FlGroup>
  );
};

export default DropdownSelect;
