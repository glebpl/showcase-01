import React, { Component, createRef } from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import getScrollbarSize from 'dom-helpers/scrollbarSize';
import { Duration } from '../../../constants';
import MenuList from '../MenuList';
import DropdownPopover from './DropdownPopover';
import { classNames } from '../../../utils/class-names';

/**
 * https://material-ui.com/demos/menus/
 */
export default class DropdownMenu extends Component {
  static propTypes = {
    /**
     * The DOM element used to set the position of the menu.
     */
    anchorEl: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
    /**
     * Menu contents, normally `MenuItem`s.
     */
    children: PropTypes.node,
    /**
     * Override or extend the styles applied to the component.
     * See [CSS API](#css-api) below for more details.
     */
    // : PropTypes.object.isRequired,
    /**
         * If `true`, the selected / first menu item will not be auto focused.
         */
    disableAutoFocusItem: PropTypes.bool
    /**
         * Properties applied to the [`MenuList`](/api/menu-list/) element.
         */
    , menuListProps: PropTypes.object
    /**
         * Callback fired when the component requests to be closed.
         *
         * @param {object} event The event source of the callback
         * @param {string} reason Can be:`"escapeKeyDown"`, `"backdropClick"`, `"tabKeyDown"`
         */
    , onClose: PropTypes.func,
    /**
         * Callback fired before the Menu enters.
         */
    onEnter: PropTypes.func,
    /**
         * Callback fired when the Menu has entered.
         */
    onEntered: PropTypes.func,
    /**
         * Callback fired when the Menu is entering.
         */
    onEntering: PropTypes.func,
    /**
         * Callback fired before the Menu exits.
         */
    onExit: PropTypes.func,
    /**
         * Callback fired when the Menu has exited.
         */
    onExited: PropTypes.func,
    /**
         * Callback fired when the Menu is exiting.
         */
    onExiting: PropTypes.func,
    /**
         * If `true`, the menu is visible.
         */
    open: PropTypes.bool.isRequired
    /**
         * The length of the transition in `ms`, or 'auto'
         */
    , transitionDuration: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.shape({ enter: PropTypes.number, exit: PropTypes.number }),
      PropTypes.oneOf(['auto'])
    ])
  };

  static defaultProps = {
    disableAutoFocusItem: false
    , menuListProps: {}
    , transitionDuration: Duration.STANDARD
  };

  _menuListRef = createRef();

  get menuListRef() {
    return this._menuListRef.current;
  }

  componentDidMount() {
    if (this.props.open && this.props.disableAutoFocusItem !== true) {
      this.focus();
    }
  }

  _getContentAnchorEl = () => {
    if (this.menuListRef.selectedItemRef) {
      return findDOMNode(this.menuListRef.selectedItemRef);
    }

    return findDOMNode(this.menuListRef).firstChild;
  };

  focus = () => {
    if (this.menuListRef && this.menuListRef.selectedItemRef) {
      findDOMNode(this.menuListRef.selectedItemRef).focus();
      return;
    }

    const menuList = findDOMNode(this.menuListRef);
    if (menuList && menuList.firstChild) {
      menuList.firstChild.focus();
    }
  };

  _handleEntering = el => {
    const {disableAutoFocusItem} = this.props;
    const menuList = findDOMNode(this.menuListRef);

    // Focus so the scroll computation of the Popover works as expected.
    if (disableAutoFocusItem !== true) {
      this.focus();
    }

    // Let's ignore that piece of logic if users are already overriding the width
    // of the menu.
    if (menuList && el.clientHeight < menuList.clientHeight && !menuList.style.width) {
      const size = `${getScrollbarSize()}px`;
      menuList.style.paddingRight = size;
      menuList.style.width = `calc(100% + ${size})`;
    }

    if (this.props.onEntering) {
      this.props.onEntering(el);
    }
  };

  _handleListKeyDown = (event, key) => {
    if (key === 'tab') {
      event.preventDefault();

      if (this.props.onClose) {
        this.props.onClose(event, 'tabKeyDown');
      }
    }
  };

  render() {
    const {
      children
      , disableAutoFocusItem
      , menuListProps: {className: menuClassName, ...restMenuProps}
      , onEntering
      , ...rest
    } = this.props;

    return (
      <DropdownPopover
        getContentAnchorEl={this._getContentAnchorEl}
        onEntering={this._handleEntering}
        {...rest}
      >
        <MenuList
          onKeyDown={this._handleListKeyDown}
          className={classNames(['dropdown-menu', menuClassName])}
          {...restMenuProps}
          ref={this._menuListRef}
        >
          {children}
        </MenuList>
      </DropdownPopover>
    );
  }
}




