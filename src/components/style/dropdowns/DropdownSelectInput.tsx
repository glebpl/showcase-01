import React, {
  Fragment,
  useImperativeHandle,
  forwardRef,
  useState,
  useRef,
  useEffect,
  useCallback,
  HTMLAttributes, AllHTMLAttributes
} from 'react';
import { useForkRef } from '../../../utils/react-helpers';
import { classNames } from '../../../utils/class-names';
import { Cls } from '../../../constants';
import DropdownMenu from './DropdownMenu';
import DropdownItem from './DropdownItem';
import DropdownCaret from './DropdownCaret';

const noop = () => {};
const hasValue = (value): boolean => value != null && !(Array.isArray(value) && value.length === 0);
// const isEmpty = display => display == null || (typeof display === 'string' && !display.trim());

// Determine if field is empty or filled.
// Response determines if label is presented above field or as placeholder.
//
// @param obj
// @param SSR
// @returns {boolean} False when not present or empty string.
//                    True when any number or string with length.
/*const isFilled = (obj, SSR = false) => {
    return (
        obj &&
        ((hasValue(obj.value) && obj.value !== '') ||
            (SSR && hasValue(obj.defaultValue) && obj.defaultValue !== ''))
    );
};*/

const areEqualValues = (a, b): boolean => {
  if(typeof b === 'object' && b !== null) {
    return a === b;
  }
  return String(a) === String(b);
};

type InputRefType = React.RefObject<HTMLInputElement>;

interface MenuItem {
  value: string;
}

interface DisplayProps extends HTMLAttributes<HTMLDivElement>{}

export interface DropdownSelectInputProps extends Omit<AllHTMLAttributes<HTMLInputElement>, 'onChange'> {
  autoWidth?: boolean;
  formatValue?: (v: number | string) => string;
  iconComponent?: React.ElementType;
  inputRef?: InputRefType;
  menuItems?: MenuItem[];
  onChange?: (e: Event, item: MenuItem) => void;
  onClose?: Function;
  onOpen?: Function;
  selectDisplayProps?: DisplayProps;
}

const DropdownSelectInput = forwardRef(function DropdownSelectInput(props: DropdownSelectInputProps, ref) {
  const {
    autoFocus = false
    , autoWidth = false
    , children
    , disabled = false
    , formatValue = v => v
    , iconComponent: IconComponent = DropdownCaret
    , inputRef: inputRefProp
    , menuItems
    , multiple = false// !!! not complete
    , name
    , onBlur = noop
    , onChange = noop
    , onClose = noop
    , onFocus = noop
    , onOpen = noop
    , open: openProp
    , readOnly = false
    , tabIndex: tabIndexProp
    , selectDisplayProps = {}
    , value
    , ...inputProps
  } = props;

  const inputRef: InputRefType = useRef(null);
  const displayRef: React.RefObject<HTMLDivElement> = useRef(null);
  const ignoreNextBlur = useRef(false);
  const handleRef = useForkRef(ref, inputRefProp);
  const {current: isOpenControlled} = useRef(openProp != null);
  const [menuMinWidthState, setMenuMinWidthState] = useState(null);
  const [openState, setOpenState] = useState(false);
  const [, forceUpdate] = useState(false);

  useImperativeHandle(handleRef, () => ({
    focus: () => displayRef.current.focus()
    , node: inputRef.current
    , value
  }), [value]);

  useEffect(() => {
    if (isOpenControlled && openProp) {
      // Focus the display node so the focus is restored on this element once
      // the menu is closed.
      displayRef.current.focus();
      // Rerender with the resolve `displayRef` reference.
      forceUpdate(n => !n);
    }

    if (autoFocus) {
      displayRef.current.focus();
    }
  }, [autoFocus, isOpenControlled, openProp]);

  const update = useCallback((open, event) => {
    if (open) {
      if (onOpen) {
        onOpen(event);
      }
    } else if (onClose) {
      onClose(event);
    }

    if (!isOpenControlled) {
      setMenuMinWidthState(autoWidth ? null : displayRef.current.clientWidth);
      setOpenState(open);
    }
  }, [onOpen, onClose, autoWidth, isOpenControlled, displayRef]);

  const handleKeyDown = useCallback(event => {
    if (!readOnly) {
      const validKeys = [
        ' '
        , 'ArrowUp'
        , 'ArrowDown'
        // The native select doesn't respond to enter on MacOS, but it's recommended by
        // https://www.w3.org/TR/wai-aria-practices/examples/listbox/listbox-collapsible.html
        , 'Enter'
      ];

      if (validKeys.indexOf(event.key) !== -1) {
        event.preventDefault();
        // Opening the menu is going to blur the. It will be focused back when closed.
        ignoreNextBlur.current = true;
        update(true, event);
      }
    }
  }, [readOnly, ignoreNextBlur, update]);

  const handleBlur = useCallback(event => {
    if (ignoreNextBlur.current === true) {
      // The parent components are relying on the bubbling of the event.
      event.stopPropagation();
      ignoreNextBlur.current = false;
      return;
    }

    if (onBlur) {
      event.persist();
      event.target = { value, name };
      onBlur(event);
    }
  }, [onBlur, ignoreNextBlur]);

  const handleClick = useCallback(event => {
    // Opening the menu is going to blur the. It will be focused back when closed.
    ignoreNextBlur.current = true;
    update(true, event);
  }, [ignoreNextBlur, update]);

  const handleClose = event => {
    update(false, event);
  };

  const handleItemClick = item => event => {
    if (!multiple) {
      update(false, event);
    }
    if (onChange) {
      let newValue;

      if (multiple) {
        // not complete, see original
      } else {
        newValue = item.value;
      }

      event.persist();
      event.target = { value: newValue, name };
      onChange(event, item);
    }
  };

  // Avoid performing a layout computation in the render method.
  let menuMinWidth = menuMinWidthState;

  if (!autoWidth && isOpenControlled && displayRef.current) {
    // use it in menuListProps
    menuMinWidth = displayRef.current.clientWidth;
  }

  let tabIndex;
  if (typeof tabIndexProp !== 'undefined') {
    tabIndex = tabIndexProp;
  } else {
    tabIndex = disabled ? null : 0;
  }

  const open = isOpenControlled && displayRef.current ? openProp : openState;

  delete inputProps['aria-invalid'];

  let display = formatValue(value);

  return (
    <Fragment>
      <div
        className={classNames({
          [Cls.DISABLED]: disabled
        })}
        ref={displayRef}
        tabIndex={tabIndex}
        role="button"
        aria-expanded={open ? 'true' : undefined}
        aria-haspopup="listbox"
        aria-owns={open ? `menu-${name || ''}` : undefined}
        onKeyDown={handleKeyDown}
        onBlur={handleBlur}
        onClick={disabled || readOnly ? null : handleClick}
        onFocus={onFocus}
        {...selectDisplayProps}
      >
        {display}
      </div>
      <input
        value={Array.isArray(value) ? value.join(',') : value}
        name={name}
        ref={inputRef}
        type="hidden"
        autoFocus={autoFocus}
        {...inputProps}
      />
      <IconComponent />
      <DropdownMenu
        anchorEl={displayRef.current}
        className="dropdown-select"
        open={open}
        onClose={handleClose}
        menuListProps={{
          role: 'listbox'
          , style: {
            minWidth: menuMinWidth
          }
        }}
      >
        {menuItems.map(item => {
          const selected = areEqualValues(value, item.value);
          return (
            <DropdownItem
              key={item.value}
              aria-selected={selected ? 'true' : undefined}
              selected={selected}
              onClick={handleItemClick(item)}
              role="option"
              data-value={item.value}
            >
              {formatValue(item.value)}
            </DropdownItem>
          );
        })}
      </DropdownMenu>
    </Fragment>
  );
});



export default DropdownSelectInput;
