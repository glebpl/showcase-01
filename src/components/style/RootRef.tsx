import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';

const setRef = (ref, value) => {
  if (typeof ref === 'function') {
    ref(value);
  } else if (ref) {
    ref.current = value;
  }
};

/**
 * Taken from mui
 */
export interface RootRefProps<T = any> {
  rootRef?: ((instance: T | null) => void) | React.RefObject<T>;
}

export default class RootRef extends Component<RootRefProps> {
  public ref;

  componentDidMount() {
    this.ref = findDOMNode(this);
    setRef(this.props.rootRef, this.ref);
  }

  componentDidUpdate(prevProps) {
    const ref = findDOMNode(this);
    const {rootRef} = this.props;

    if (prevProps.rootRef !== rootRef || this.ref !== ref) {
      if (prevProps.rootRef !== rootRef) {
        setRef(prevProps.rootRef, null);
      }

      this.ref = ref;

      setRef(rootRef, this.ref);
    }
  }

  componentWillUnmount() {
    this.ref = null;
    setRef(this.props.rootRef, null);
  }

  render() {
    return this.props.children;
  }
}
