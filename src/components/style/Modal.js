import React, { Component, cloneElement, createRef } from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';
import keycode from 'keycode';
import getScrollbarSize from 'dom-helpers/scrollbarSize';
import { createChainedFunction } from '../../utils/functions';
import { classNames } from '../../utils/class-names';
import { domOwnerDocument, domIsOverflowing } from '../../utils/dom';
import Portal from './Portal';
import Backdrop from './Backdrop';
import RootRef from './RootRef';

// TODO CLASS_MUI_FIXED remove this hack
// class used to set padding = scrollbar size when backdrop added, see Modal
const CLASS_MUI_FIXED = 'mui-fixed';

const getPaddingRight = el => parseInt(el.style.paddingRight || 0);

const setContainerStyle = data => {
  const style = { overflow: 'hidden' };

  // We are only interested in the actual `style` here because we will override it.
  data.style = {
    overflow: data.container.style.overflow,
    paddingRight: data.container.style.paddingRight,
  };

  if (data.overflowing) {
    const scrollbarSize = getScrollbarSize();

    // Use computed style, here to get the real padding to add our scrollbar width.

    style.paddingRight = `${getPaddingRight(data.container) + scrollbarSize}px`;

    // .mui-fixed is a global helper.
    const fixedNodes = domOwnerDocument(data.container).querySelectorAll(`.${CLASS_MUI_FIXED}`);
    for (let i = 0; i < fixedNodes.length; i += 1) {
      const paddingRight = getPaddingRight(fixedNodes[i]);
      data.prevPaddings.push(paddingRight);
      // @ts-ignore
      fixedNodes[i].style.paddingRight = `${paddingRight + scrollbarSize}px`;
    }
  }

  Object.keys(style).forEach(key => {
    data.container.style[key] = style[key];
  });
};

const removeContainerStyle = data => {
  // The modal might be closed before it had the chance to be mounted in the DOM.
  if (data.style) {
    Object.keys(data.style).forEach(key => {
      data.container.style[key] = data.style[key];
    });
  }

  const fixedNodes = domOwnerDocument(data.container).querySelectorAll('.mui-fixed');
  for (let i = 0; i < fixedNodes.length; i += 1) {
    // @ts-ignore
    fixedNodes[i].style.paddingRight = `${data.prevPaddings[i]}px`;
  }
};

const getContainer = (container, defaultContainer) => {
  container = typeof container === 'function' ? container() : container;
  return findDOMNode(container) || defaultContainer;
};

const getHasTransition = (props) => props.children ? props.children.props.hasOwnProperty('in') : false;

/**
 * Taken from materual-ui and refactored
 * Proper state managment for containers and the modals in those containers.
 * Simplified, but inspired by react-overlay's ModalManager class.
 * Used by the Modal to ensure proper styling of containers.
 */
class ModalManager {
  constructor(options = {}) {
    const {
      hideSiblingNodes = true
      , handleContainerOverflow = true
    } = options;

    this.hideSiblingNodes = hideSiblingNodes;
    this.handleContainerOverflow = handleContainerOverflow;

    this.modals = [];
    this.data = [];
  }

  add(modal, container) {
    let modalIdx = this.modals.indexOf(modal);
    if (modalIdx !== -1) {
      return modalIdx;
    }

    modalIdx = this.modals.length;

    this.modals.push(modal);

    // If the modal we are adding is already in the DOM.
    /* excluded if (modal.wrapperEl) {
            ariaHidden(modal.wrapperEl, false);
        }*/

    /* excluded if (this.hideSiblingNodes) {
            ariaHiddenSiblings(container, modal.parentEl, modal.wrapperEl, true);
        }*/

    const containerIdx = this.data.findIndex(item => item.container === container);

    if (containerIdx !== -1) {
      this.data[containerIdx].modals.push(modal);
      return modalIdx;
    }

    const data = {
      modals: [modal],
      container,
      overflowing: domIsOverflowing(container),
      prevPaddings: [],
    };

    this.data.push(data);

    return modalIdx;
  }

  mount(modal) {
    const containerIdx = this.data.findIndex(item => item.modals.indexOf(modal) !== -1);
    const data = this.data[containerIdx];

    if (!data.style && this.handleContainerOverflow) {
      setContainerStyle(data);
    }
  }

  remove(modal) {
    const modalIdx = this.modals.indexOf(modal);

    if (modalIdx === -1) {
      return modalIdx;
    }

    const containerIdx = this.data.findIndex(item => item.modals.indexOf(modal) !== -1);
    const data = this.data[containerIdx];

    data.modals.splice(data.modals.indexOf(modal), 1);

    this.modals.splice(modalIdx, 1);

    // If that was the last modal in a container, clean up the container.
    if (data.modals.length === 0) {
      if (this.handleContainerOverflow) {
        removeContainerStyle(data);
      }

      // In case the modal wasn't in the DOM yet.
      /* excluded if (modal.wrapperEl) {
                ariaHidden(modal.wrapperEl, true);
            }*/

      /* excluded if (this.hideSiblingNodes) {
                ariaHiddenSiblings(data.container, modal.parentEl, modal.wrapperEl, false);
            }*/

      this.data.splice(containerIdx, 1);
    } else if (this.hideSiblingNodes) {
      // Otherwise make sure the next top modal is visible to a screen reader.

      // as soon as a modal is adding its el is undefined. it can't set
      // aria-hidden because the dom element doesn't exist either
      // when modal was unmounted before el gets null

      /* excluded
            const nextTop = data.modals[data.modals.length - 1];
            if (nextTop.wrapperEl) {
                ariaHidden(nextTop.wrapperEl, false);
            }*/
    }

    return modalIdx;
  }

  isTopModal(modal) {
    return !!this.modals.length && this.modals[this.modals.length - 1] === modal;
  }
}

/**
 * Taken from materual-ui and refactored
 * https://material-ui.com/utils/modal/#modal
 * This component shares many concepts with [react-overlays](https://react-bootstrap.github.io/react-overlays/#modals).
 */
export default class Modal extends Component {
  static propTypes = {
    /**
     * Properties applied to the [`Backdrop`](/api/backdrop/) element.
     */
    BackdropProps: PropTypes.object
    /**
     * A single child content element.
     */
    , children: PropTypes.element

    , className: PropTypes.string

    /**
     * A node, component instance, or function that returns either.
     * The `container` will have the portal children appended to it.
     */
    , container: PropTypes.oneOfType([PropTypes.object, PropTypes.func])

    /**
     * If `true`, the modal will not automatically shift focus to itself when it opens, and
     * replace it to the last focused element when it closes.
     * This also works correctly with any modal children that have the `disableAutoFocus` prop.
     *
     * Generally this should never be set to `true` as it makes the modal less
     * accessible to assistive technologies, like screen readers.
     */
    , disableAutoFocus: PropTypes.bool

    /**
     * If `true`, clicking the backdrop will not fire any callback.
     */
    , disableBackdropClick: PropTypes.bool

    /**
     * If `true`, the modal will not prevent focus from leaving the modal while open.
     *
     * Generally this should never be set to `true` as it makes the modal less
     * accessible to assistive technologies, like screen readers.
     */
    , disableEnforceFocus: PropTypes.bool

    /**
     * If `true`, hitting escape will not fire any callback.
     */
    , disableEscapeKeyDown: PropTypes.bool

    /**
     * Disable the portal behavior.
     * The children stay within it's parent DOM hierarchy.
     */
    , disablePortal: PropTypes.bool

    /**
     * If `true`, the modal will not restore focus to previously focused element once
     * modal is hidden.
     */
    , disableRestoreFocus: PropTypes.bool

    /**
     * If `true`, the backdrop is not rendered.
     */
    , hideBackdrop: PropTypes.bool
    /**
     * Always keep the children in the DOM.
     * This property can be useful in SEO situation or
     * when you want to maximize the responsiveness of the Modal.
     */
    , keepMounted: PropTypes.bool
    /**
     * A modal manager used to track and manage the state of open
     * Modals. This enables customizing how modals interact within a container.
     */
    , manager: PropTypes.object

    /**
     * Callback fired when the backdrop is clicked.
     */
    , onBackdropClick: PropTypes.func

    /**
     * Callback fired when the component requests to be closed.
     * The `reason` parameter can optionally be used to control the response to `onClose`.
     *
     * @param {object} event The event source of the callback
     * @param {string} reason Can be:`"escapeKeyDown"`, `"backdropClick"`
     */
    , onClose: PropTypes.func

    /**
     * Callback fired when the escape key is pressed,
     * `disableEscapeKeyDown` is false and the modal is in focus.
     */
    , onEscapeKeyDown: PropTypes.func

    /**
     * Callback fired once the children has been mounted into the `container`.
     * It signals that the `open={true}` property took effect.
     */
    , onRendered: PropTypes.func

    /**
     * If `true`, the modal is open.
     */
    , open: PropTypes.bool.isRequired
  };

  static defaultProps = {
    disableAutoFocus: false,
    disableBackdropClick: false,
    disableEnforceFocus: false,
    disableEscapeKeyDown: false,
    disablePortal: false,
    disableRestoreFocus: false,
    hideBackdrop: false,
    keepMounted: false,
    // Modals don't open on the server so this won't conflict with concurrent requests.
    manager: new ModalManager()
  };

  static Manager = ModalManager;

  mounted = false;

  constructor(props) {
    super();

    this.state = {
      exited: !props.open,
    };

    this._portalRef = createRef();
    this._modalRef = createRef();
    this._dialogRef = createRef();
  }

  /**
     * Parent of wrapper
     * @return {*}
     */
  get parentEl() {
    // _portalRef.current contains Portal component
    return this._portalRef.current.el;
  }

  /**
     * Element containing backdrop and modal
     * Has class 'modal' as in BS
     */
  get wrapperEl() {
    return this._modalRef.current;
  }

  /**
     * Alias of wrapper El
     */
  get modalEl() {
    return this._modalRef.current;
  }

  /**
     * Modal dialog element
     */
  get dialogEl() {
    return this._dialogRef.current;
  }

  componentDidMount() {
    this.mounted = true;
    if (this.props.open) {
      this._handleOpen();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.open && !this.props.open) {
      this._handleClose();
    } else if (!prevProps.open && this.props.open) {
      this.lastFocus = domOwnerDocument(this.parentEl).activeElement;
      this._handleOpen();
    }
  }

  componentWillUnmount() {
    this.mounted = false;

    if (this.props.open || (getHasTransition(this.props) && !this.state.exited)) {
      this._handleClose();
    }
  }

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.open) {
      return {
        exited: false,
      };
    }

    if (!getHasTransition(nextProps)) {
      // Otherwise let _handleExited take care of marking exited.
      return {
        exited: true,
      };
    }

    return null;
  }

  _handleOpen = () => {
    const doc = domOwnerDocument(this.wrapperEl);
    const container = getContainer(this.props.container, doc.body);

    this.props.manager.add(this, container);
    doc.addEventListener('keydown', this._handleDocumentKeyDown);

    // excluded doc.addEventListener('focus', this.enforceFocus, true);

    if (this.wrapperEl) {
      this._handleOpened();
    }
  };

  _handleRendered = () => {
    if (this.props.onRendered) {
      this.props.onRendered();
    }

    if (this.props.open) {
      this._handleOpened();
    } else {
      /* excluded ariaHidden(this.wrapperEl, true);*/
    }
  };

  _handleOpened = () => {
    // excluded, may be not important this.autoFocus();

    this.props.manager.mount(this);

    // Fix a bug on Chrome where the scroll isn't initially 0.
    this.wrapperEl.scrollTop = 0;
  };

  _handleClose = () => {
    this.props.manager.remove(this);

    const doc = domOwnerDocument(this.wrapperEl);

    doc.removeEventListener('keydown', this._handleDocumentKeyDown);
    // excluded doc.removeEventListener('focus', this.enforceFocus, true);

    // excluded this.restoreLastFocus();
  };

  /* excluded, calls are excluded _handleExited = () => {
        this.setState({ exited: true });
    };*/

  _handleBackdropClick = event => {
    if (event.target !== event.currentTarget) {
      return;
    }

    if (this.props.onBackdropClick) {
      this.props.onBackdropClick(event);
    }

    if (!this.props.disableBackdropClick && this.props.onClose) {
      this.props.onClose(event, 'backdropClick');
    }
  };

  _handleDocumentKeyDown = event => {
    // Ignore events that have been `event.preventDefault()` marked.
    if (keycode(event) !== 'esc' || !this.isTopModal() || event.defaultPrevented) {
      return;
    }

    if (this.props.onEscapeKeyDown) {
      this.props.onEscapeKeyDown(event);
    }

    if (!this.props.disableEscapeKeyDown && this.props.onClose) {
      this.props.onClose(event, 'escapeKeyDown');
    }
  };

  /* excluded enforceFocus = () => {
        // The Modal might not already be mounted.
        if (!this.isTopModal() || this.props.disableEnforceFocus || !this.mounted || !this.dialogRef) {
            return;
        }

        const currentActiveElement = domOwnerDocument(this.wrapperEl).activeElement;

        if (!this.dialogRef.contains(currentActiveElement)) {
            this.dialogRef.focus();
        }
    };*/

  /* excluded may be not important autoFocus() {
        // We might render an empty child.
        if (this.props.disableAutoFocus || !this.dialogRef) {
            return;
        }

        const currentActiveElement = domOwnerDocument(this.wrapperEl).activeElement;

        if (!this.dialogRef.contains(currentActiveElement)) {
            if (!this.dialogRef.hasAttribute('tabIndex')) {
/!*                warning(
                    false,
                    [
                        'Material-UI: the modal content node does not accept focus.',
                        'For the benefit of assistive technologies, ' +
                        'the tabIndex of the node is being set to "-1".',
                    ].join('\n'),
                );*!/
                this.dialogRef.setAttribute('tabIndex', -1);
            }

            this.lastFocus = currentActiveElement;
            this.dialogRef.focus();
        }
    }*/

  /* excluded restoreLastFocus() {
        if (this.props.disableRestoreFocus || !this.lastFocus) {
            return;
        }

        // Not all elements in IE 11 have a focus method.
        // Because IE 11 market share is low, we accept the restore focus being broken
        // and we silent the issue.
        if (this.lastFocus.focus) {
            this.lastFocus.focus();
        }

        this.lastFocus = null;
    }*/

  isTopModal() {
    return this.props.manager.isTopModal(this);
  }

  render() {
    /* eslint-disable no-unused-vars */
    const {
      BackdropProps,
      children,
      className,
      container,
      disableAutoFocus,
      disableBackdropClick,
      disableEnforceFocus,
      disableEscapeKeyDown,
      disablePortal,
      disableRestoreFocus,
      hideBackdrop,
      keepMounted,
      manager,
      onBackdropClick,
      onClose,
      onEscapeKeyDown,
      onRendered,
      open,
      ...other
    } = this.props;
    /* eslint-enable no-unused-vars */

    const {exited} = this.state;
    const hasTransition = getHasTransition(this.props);

    if (!keepMounted && !open && (!hasTransition || exited)) {
      return null;
    }

    const childProps = {};

    // It's a Transition like component
    if (hasTransition) {
      childProps.onExited = createChainedFunction(this._handleExited, children.props.onExited);
    }

    /*if (children.props.role === undefined) {
            childProps.role = children.props.role || 'document';
        }

        if (children.props.tabIndex === undefined) {
            childProps.tabIndex = children.props.tabIndex || '-1';
        }*/

    const wrapperClassNames = classNames({
      'modal': true
      , 'invisible': exited
      , 'show': open
      , [className]: !!className
    });

    return (
      <Portal
        ref={this._portalRef}
        container={container}
        disablePortal={disablePortal}
        onRendered={this._handleRendered}
      >
        <div
          ref={this._modalRef}
          className={wrapperClassNames}
          {...other}
        >
          {hideBackdrop ? null : (
            <Backdrop
              open={open}
              onClick={this._handleBackdropClick}
              className="modal-backdrop"
              {...BackdropProps}
            />
          )}
          {/*<div className={UI.CLASS_MODAL_DIALOG} ref={this._dialogRef}>*/}
          {/*{children}*/}
          {/*</div>*/}
          <RootRef rootRef={this._dialogRef}>{cloneElement(children, childProps)}</RootRef>
        </div>
      </Portal>
    );
  }
}
