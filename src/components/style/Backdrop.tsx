import React from 'react';
import { classNames } from '../../utils/class-names';
import { Cls, Duration } from '../../constants';
import Fade, { FadeProps } from './transitions/Fade';
import { TransitionTimeout } from '../../types';

export interface BackdropProps extends FadeProps {
  invisible?: boolean;
  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  transitionDuration?: TransitionTimeout;
}

function Backdrop(props: BackdropProps) {
  const {
    className
    , invisible = false
    , open = false
    , transitionDuration = Duration.STANDARD
    , ...other
  } = props;

  const classes = classNames({
    'backdrop': true
    , [Cls.TRANSPARENT]: invisible
    , [className]: !!className
  });

  return (
    <Fade in={open} timeout={transitionDuration} {...other}>
      <div className={classes} aria-hidden="true" />
    </Fade>
  );
}

export default Backdrop;
