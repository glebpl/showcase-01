import React, { forwardRef, HTMLAttributes } from 'react';

export interface FormHelperTextProps<H = HTMLParagraphElement> extends HTMLAttributes<H> {
  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component?: React.ElementType;
}

const FormHelperText = forwardRef(function FormHelperText(props: FormHelperTextProps, ref) {
  const {
    component: Component = 'p'
    , ...other
  } = props;

  return (
    <Component
      className="fl-helper"
      ref={ref}
      {...other}
    />
  );
});

export default FormHelperText;
