import React, { AllHTMLAttributes, forwardRef, RefObject } from 'react';

export type FormProps = AllHTMLAttributes<HTMLFormElement>

export default forwardRef(function Form(props: FormProps, ref: RefObject<HTMLFormElement>) {
  const {
    method = 'post'
    , ...rest
  } = props;

  return (
    <form ref={ref} method={method} {...rest} />
  );
});
