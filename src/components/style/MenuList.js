import React, {Component, createRef, Children, isValidElement, cloneElement} from 'react';
import PropTypes from 'prop-types';
import keycode from 'keycode';
import { domOwnerDocument } from '../../utils/dom';

export default class MenuList extends Component {
  static propTypes = {
    children: PropTypes.node
    , className: PropTypes.string
    , onBlur: PropTypes.func
    , onKeyDown: PropTypes.func
  };

  constructor() {
    super();

    this.state = {
      currentTabIndex: null
    };

    this._listRef = createRef();
    this._selectedItemRef = createRef();
  }

  get listRef() {
    return this._listRef.current;
  }

  get selectedItemRef() {
    return this._selectedItemRef.current;
  }

  componentDidMount() {
    this._resetTabIndex();
  }

  componentWillUnmount() {
    clearTimeout(this.blurTimer);
  }

  setTabIndex(index) {
    this.setState({ currentTabIndex: index });
  }

  handleBlur = event => {
    this.blurTimer = setTimeout(() => {
      if (this.listRef) {
        const list = this.listRef;
        const currentFocus = domOwnerDocument(list).activeElement;
        if (!list.contains(currentFocus)) {
          this._resetTabIndex();
        }
      }
    }, 30);

    if (this.props.onBlur) {
      this.props.onBlur(event);
    }
  };

  handleKeyDown = event => {
    const list = this.listRef;
    const key = keycode(event);
    const currentFocus = domOwnerDocument(list).activeElement;

    if (
      (key === 'up' || key === 'down') &&
            (!currentFocus || (currentFocus && !list.contains(currentFocus)))
    ) {
      if (this.selectedItemRef) {
        this.selectedItemRef.focus();
      } else {
        list.firstChild.focus();
      }
    } else if (key === 'down') {
      event.preventDefault();
      if (currentFocus.nextElementSibling) {
        currentFocus.nextElementSibling.focus();
      }
    } else if (key === 'up') {
      event.preventDefault();
      if (currentFocus.previousElementSibling) {
        currentFocus.previousElementSibling.focus();
      }
    }

    if (this.props.onKeyDown) {
      this.props.onKeyDown(event, key);
    }
  };

  _handleItemFocus = event => {
    const list = this.listRef;
    if (list) {
      for (let i = 0; i < list.children.length; i += 1) {
        if (list.children[i] === event.currentTarget) {
          this.setTabIndex(i);
          break;
        }
      }
    }
  };

  focus() {
    const {currentTabIndex} = this.state;
    const list = this.listRef;
    if (!list || !list.children || !list.firstChild) {
      return;
    }

    if (currentTabIndex && currentTabIndex >= 0) {
      list.children[currentTabIndex].focus();
    } else {
      list.firstChild.focus();
    }
  }

  _resetTabIndex() {
    const list = this.listRef;
    const currentFocus = domOwnerDocument(list).activeElement;

    const items = [];
    for (let i = 0; i < list.children.length; i += 1) {
      items.push(list.children[i]);
    }

    const currentFocusIndex = items.indexOf(currentFocus);

    if (currentFocusIndex !== -1) {
      return this.setTabIndex(currentFocusIndex);
    }

    if (this.selectedItemRef) {
      return this.setTabIndex(items.indexOf(this.selectedItemRef));
    }

    return this.setTabIndex(0);
  }

  render() {
    const { children, className, onBlur, onKeyDown, ...other } = this.props;

    return (
      <div
        role="menu"
        ref={this._listRef}
        className={className}
        onKeyDown={this.handleKeyDown}
        onBlur={this.handleBlur}
        {...other}
      >
        {Children.map(children, (child, index) => {
          if (!isValidElement(child)) {
            return null;
          }

          /*warning(
                        child.type !== React.Fragment,
                        [
                            "Material-UI: the MenuList component doesn't accept a Fragment as a child.",
                            'Consider providing an array instead.',
                        ].join('\n'),
                    );*/

          return cloneElement(child, {
            tabIndex: index === this.state.currentTabIndex ? 0 : -1
            , ref: child.props.selected ? this._selectedItemRef : undefined
            , onFocus: this._handleItemFocus
          });
        })}
      </div>
    );
  }
}
