import React, { forwardRef, HTMLAttributes } from 'react';
import { classNames } from '../../utils/class-names';

export type FlGroupRefType = React.RefObject<HTMLDivElement>
export interface FlGroupProps extends HTMLAttributes<HTMLDivElement> {}

const FlGroup = forwardRef(function FlGroup(props: FlGroupProps, ref: FlGroupRefType) {
  const {
    className = ''
    , ...rest
  } = props;

  return (
    <div
      ref={ref}
      className={classNames(['fl-group', className])}
      {...rest}
    />
  );
});

export default FlGroup;
