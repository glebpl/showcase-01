import React, { HTMLAttributes } from 'react';
import { classNames } from '../../utils/class-names';

interface SpinnerProps extends HTMLAttributes<HTMLDivElement> {}

const Spinner: React.FC<SpinnerProps> = ({className = ''}) => (
  <div className={classNames(['spinner', className])} />
);

export default Spinner;
