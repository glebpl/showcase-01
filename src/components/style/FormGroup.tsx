import React, { forwardRef } from 'react';
import { classNames } from '../../utils/class-names';

const FormGroup = (props, ref) => {
  const {className} = props;
  return (
    <div
      {...props}
      ref={ref}
      className={classNames(['form-group', className])}
    />
  );
};

export default forwardRef(FormGroup);
