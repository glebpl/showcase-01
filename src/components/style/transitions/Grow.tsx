import React, { Component, cloneElement } from 'react';
import Transition from 'react-transition-group/Transition';
import { domReflow, domCreateTransitions, domGetTransitionProps, domGetAutoHeightDuration } from '../../../utils/dom';
import { TransitionComponentProps } from './types';
import { mergeChildrenStyle } from './utils';
import { Timeout } from '../../../types';

const getScale = value => `scale(${value}, ${value ** 2})`;

const styles = {
  entering: {
    opacity: 1
    , transform: getScale(1)
  }
  , entered: {
    opacity: 1
    // Use translateZ to scrolling issue on Chrome.
    , transform: `${getScale(1)} translateZ(0)`
  }
};

// Omit to prevent incompatible types
export type GrowProps = TransitionComponentProps;

/**
 * https://material-ui.com/api/grow/
 * The Grow transition is used by the [Tooltip](/demos/tooltips/) and
 * [Popover](/utils/popover/) components.
 * It uses [react-transition-group](https://github.com/reactjs/react-transition-group) internally.
 */
export default class Grow extends Component<GrowProps> {
  autoTimeout: number;
  timer: Timeout;

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  handleEnter = el => {
    const {timeout} = this.props;
    let duration = 0;

    domReflow(el); // So the animation always start from the start.

    const { duration: transitionDuration, delay } = domGetTransitionProps(this.props, {
      mode: 'enter',
    });

    if (timeout === 'auto') {
      duration = domGetAutoHeightDuration(el.clientHeight);
      this.autoTimeout = duration;
    } else {
      duration = Number(transitionDuration);
    }

    el.style.transition = [
      domCreateTransitions('opacity', {
        duration
        , delay
      })
      , domCreateTransitions('transform', {
        duration: duration * 0.666
        , delay
      })
    ].join(',');

    if (this.props.onEnter) {
      this.props.onEnter(el);
    }
  };

  handleExit = node => {
    const {timeout} = this.props;
    let duration = 0;

    const { duration: transitionDuration, delay } = domGetTransitionProps(this.props, {
      mode: 'exit'
    });

    if (timeout === 'auto') {
      duration = domGetAutoHeightDuration(node.clientHeight);
      this.autoTimeout = duration;
    } else {
      duration = Number(transitionDuration);
    }

    node.style.transition = [
      domCreateTransitions('opacity', {
        duration
        , delay
      }),
      domCreateTransitions('transform', {
        duration: duration * 0.666
        , delay: delay || duration * 0.333
      })
    ].join(',');

    node.style.opacity = '0';
    node.style.transform = getScale(0.75);

    if (this.props.onExit) {
      this.props.onExit(node);
    }
  };

  addEndListener = (_, next) => {
    if (this.props.timeout === 'auto') {
      this.timer = setTimeout(next, this.autoTimeout || 0);
    }
  };

  render() {
    const {
      children
      , onEnter
      , onExit
      , style: styleProp
      , timeout = 'auto'
      , ...other
    } = this.props;

    const style = mergeChildrenStyle(styleProp, children);

    return (
      <Transition
        appear
        onEnter={this.handleEnter}
        onExit={this.handleExit}
        addEndListener={this.addEndListener}
        timeout={timeout === 'auto' ? null : timeout}
        {...other}
      >
        {(state, childProps) => {
          return cloneElement(children, {
            style: {
              opacity: 0,
              transform: getScale(0.75),
              ...styles[state],
              ...style,
            },
            ...childProps,
          });
        }}
      </Transition>
    );
  }
}

// what is it for ??? Grow.muiSupportAuto = true;
