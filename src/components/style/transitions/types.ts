import React from 'react';
import { TransitionProps } from 'react-transition-group/Transition';
import { TransitionTimeout } from '../../../types';

export type TransitionGroupTimeout = number | { appear?: number; enter?: number; exit?: number };

export interface TransitionComponentProps extends Omit<TransitionProps, 'timeout'> {
  children?: React.ReactElement
  className?: string;
  component?: React.ElementType;
  style?: CSSStyleDeclaration;
  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   *
   * Set to 'auto' to automatically calculate transition time based on height.
   */
  timeout?: TransitionTimeout;
}

// export type TransitionStylePartial = Pick<CSSStyleDeclaration, 'transitionDelay' | 'transitionDuration'>;
