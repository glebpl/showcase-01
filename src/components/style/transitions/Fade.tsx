import React, { Component, cloneElement } from 'react';
import { Transition } from 'react-transition-group';
import { domCreateTransitions, domGetTransitionProps, domReflow } from '../../../utils/dom';
import { TransitionComponentProps, TransitionGroupTimeout } from './types';
import { Duration } from '../../../constants';
import { mergeChildrenStyle } from './utils';

const styles = {
  entering: {
    opacity: 1
  }
  , entered: {
    opacity: 1
  }
};

const DEFAULT_FADE_TIMEOUT_PROPS = {
  enter: Duration.ENTERING
  , exit: Duration.LEAVING
};

export interface FadeProps extends TransitionComponentProps {}

/**
 * The Fade transition is used by the Modal component.
 * It uses [react-transition-group](https://github.com/reactjs/react-transition-group) internally.
 */
export default class Fade extends Component<FadeProps> {

  handleEnter = el => {
    domReflow(el); // So the animation always start from the start.

    const transitionProps = domGetTransitionProps(this.props, {
      mode: 'enter',
    });

    el.style.webkitTransition = domCreateTransitions('opacity', transitionProps);
    el.style.transition = domCreateTransitions('opacity', transitionProps);

    if (this.props.onEnter) {
      this.props.onEnter(el, true);
    }
  };

  handleExit = el => {
    const transitionProps = domGetTransitionProps(this.props, {
      mode: 'exit',
    });

    el.style.webkitTransition = domCreateTransitions('opacity', transitionProps);
    el.style.transition = domCreateTransitions('opacity', transitionProps);

    if (this.props.onExit) {
      this.props.onExit(el);
    }
  };

  render() {
    const {
      children
      , onEnter
      , onExit
      , style: styleProp
      , timeout = DEFAULT_FADE_TIMEOUT_PROPS
      , ...rest
    } = this.props;

    const style = mergeChildrenStyle(styleProp, children);

    return (
      <Transition
        appear
        onEnter={this.handleEnter}
        onExit={this.handleExit}
        timeout={timeout as TransitionGroupTimeout}
        {...rest}
      >
        {(state, childProps) => {
          return cloneElement(children as React.ReactElement, {
            style: {
              opacity: 0,
              willChange: 'opacity',
              ...styles[state],
              ...style
            },
            ...childProps
          });
        }}
      </Transition>
    );
  }
}
