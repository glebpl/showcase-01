import { isValidElement } from 'react';

export const mergeChildrenStyle = (styleProp, children) => {
  let style;

  if(isValidElement(children)) {
    let {style: childStyle = {}} = children.props as {style?: CSSStyleDeclaration};
    style = {
      ...styleProp
      , ...childStyle
    };
  } else {
    style = styleProp;
  }

  return style;
};
