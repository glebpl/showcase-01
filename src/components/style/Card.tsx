import React, { forwardRef, HTMLAttributes, Ref } from 'react';
import { classNames } from '../../utils/class-names';

export type CardProps = HTMLAttributes<HTMLDivElement>;

const Card: React.FC<CardProps> = (props, ref: Ref<HTMLDivElement>) => {
  const {className, ...rest} = props;
  return (
    <div ref={ref} className={classNames(['card', className])} {...rest} />
  );
};

export default forwardRef(Card);
