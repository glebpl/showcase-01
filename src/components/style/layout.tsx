import React from 'react';
import { classNames } from '../../utils/class-names';

export const LayoutContent = ({children}) => (
  <div className="layout-content">{children}</div>
);

export const LayoutMain = ({className = '', ...rest}) => (
  <main {...rest} className={classNames(['layout-main', className])}/>
);
