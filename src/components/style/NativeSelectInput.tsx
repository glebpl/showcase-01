import React, { HTMLAttributes } from 'react';
import DropdownCaret from './dropdowns/DropdownCaret';

type RefType = React.RefObject<HTMLSelectElement>;

export interface NativeSelectInputProps extends HTMLAttributes<HTMLSelectElement> {
  /**
   * The icon that displays the arrow.
   */
  IconComponent?: React.ComponentType;
  inputRef?: RefType;
}

const NativeSelectInput = React.forwardRef(function NativeSelectInput(props: NativeSelectInputProps, ref: RefType) {
  const {
    className = ''
    , IconComponent = DropdownCaret
    , inputRef
    , ...other
  } = props;

  return (
    <React.Fragment>
      <select
        className={className}
        // disabled={disabled}
        ref={inputRef || ref}
        {...other}
      />
      {IconComponent ? (
        <IconComponent />
      ) : null}
      {/*{props.multiple ? null : (
                <IconComponent className={clsx(classes.icon, classes[`icon${capitalize(variant)}`])}/>
            )}*/}
    </React.Fragment>
  );
});

export default NativeSelectInput;
