import React, { ComponentFactory } from 'react';
import { withClassNames } from '../hoc/class-names';
import Spinner from './Spinner';
import { classNames } from '../../utils/class-names';
import { ucFirst } from '../../utils/string';

const Svg = ({size = 24, className = '', ...rest}) => (
  <svg
    className={classNames(['icon', className])}
    width={`${size}px`}
    height={`${size}px`}
    viewBox={`0 0 ${size} ${size}`}
    {...rest}
  />
);

export const asIcon = withClassNames('icon');

export const makeFromFile = (fileName: string) => {
  const SvgContent = require(`../../icons/${fileName}.svg`).default;

  const fn = props => (
    <Svg {...props}>
      <SvgContent />
    </Svg>
  );

  fn.displayName = `Icon${fileName.split('-').map(ucFirst).join('')}`;

  return fn;
};

export const IconCaret = makeFromFile('caret');
export const IconVisible = makeFromFile('visible');
export const IconNotVisible = makeFromFile('not-visible');
export const IconPerson = makeFromFile('person');
export const IconSignOut = makeFromFile('sign-out');
export const IconUser = IconPerson;// alias


// Other components with 'icon' className added
export const IconSpinner = asIcon(Spinner);





