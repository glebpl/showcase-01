import React, { HTMLAttributes, RefObject, forwardRef } from 'react';
import { withClassNames } from '../hoc/class-names';
import { classNames } from '../../utils/class-names';

export type BtnProps = HTMLAttributes<HTMLButtonElement>;

export const Btn = forwardRef(function Btn(props: BtnProps, ref: RefObject<HTMLButtonElement>) {
  const {className, ...rest} = props;
  return (
    <button
      ref={ref}
      type="button"
      {...rest}
      className={classNames(['btn', className])}
    />
  );
});

export const BtnPrimary = withClassNames('btn-primary')(Btn);
export const BtnSecondary = withClassNames('btn-secondary')(Btn);
export const BtnSuccess = withClassNames('btn-success')(Btn);
export const BtnDanger = withClassNames('btn-danger')(Btn);
export const BtnLink = withClassNames('btn-link')(Btn);
export const BtnIcon = withClassNames('btn-icon')(Btn);
export const BtnIconSm = withClassNames('btn-icon-sm')(BtnIcon);
export const BtnBlockPrimary = withClassNames('btn-block')(BtnPrimary);
