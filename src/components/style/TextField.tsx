import React, { PureComponent, createRef, forwardRef, HTMLAttributes } from 'react';
import { Cls } from '../../constants';
import { classNames, createClassToggle } from '../../utils/class-names';
import { domBind } from '../../utils/dom';
import TextareaAutosize, { TextareaAutosizeProps } from './TextareaAutosize';
import FlGroup, { FlGroupRefType } from './FlGroup';
import FormHelperText from './FlHelperText';
import NativeSelectInput from './NativeSelectInput';

const _toggleFocused = createClassToggle(Cls.FOCUSED);
const _toggleDisabled = createClassToggle(Cls.DISABLED);
const _toggleFilled = createClassToggle(Cls.FILLED);
const _toggleError = createClassToggle(Cls.HAS_ERROR);

export interface TextFieldProps
  extends HTMLAttributes<HTMLInputElement>, Pick<TextareaAutosizeProps, 'rows' | 'rowsMax'> {
  /**
   * Btn to be added at the right
   */
  addon?: React.ReactElement;
  autoComplete?: boolean | string;
  autoFocus?: boolean;
  className?: string;
  disabled?: boolean;
  error?: boolean;
  /**
   * The component used for the `input` element.
   * Either a string to use a DOM element or a component.
   */
  inputComponent?: React.ElementType;
  inputRef?: React.RefObject<any>;
  helperText?: React.ReactElement;
  label?: string;
  maxLength?: number;
  /**
   * If `true`, a textarea element will be rendered instead of an input.
   */
  multiline?: boolean;
  name?: string;
  /**
   * Number of rows to display when multiline option is set to true.
   */
  // rows?: string | number;
  /**
   * Maximum number of rows to display when multiline option is set to true.
   */
  // rowsMax?: string | number;
  /**
   * Render a 'dropdown' element
   * If this option is set you must pass the options of the select as children.
   */
  select?: boolean;
  /**
   * Props applied to the 'dropdown' element.
   */
  // selectProps: {}
  type?: string;
  value?: string | number | boolean;
}

/**
 * Based on TextField of material-design-lite
 * https://getmdl.io/components/index.html#textfields-section
 * Validation not applied
 */
class TextFieldClass extends PureComponent<TextFieldProps> {
  static defaultProps = {
    type: 'text'
    , addon: null
    , inputComponent: 'input'
    , className: ''
    , label: ''
    , maxLength: 255
    , multiline: false
    , rows: 1
    , rowsMax: 4
    , select: false
    , autoFocus: false
    , error: false
  };

  readonly _wrapperRef: FlGroupRefType = createRef();

  get container() {
    return this._wrapperRef.current ? this._wrapperRef.current : null;
  }

  get input() {
    return this.props.inputRef.current;
  }

  componentDidMount() {
    const el = this.input;

    domBind(el, 'input', this._updateClasses);
    domBind(el, 'focus', this._onFocus);
    domBind(el, 'blur', this._onBlur);
    domBind(el, 'reset', this._updateClasses);

    this._updateClasses();

    if (this.props.autoFocus) {
      el.focus();
    }
  }

  componentDidUpdate(prevProps) {
    if(prevProps.disabled !== this.props.disabled) {
      this._checkDisabled();
    }
    if(prevProps.error !== this.props.error) {
      this._checkError();
    }
  }

  render() {
    const {
      className
      , error
      , addon
      , inputComponent
      , inputRef
      , helperText
      , label
      , maxLength
      , multiline
      , rows
      , rowsMax
      , select
      // , selectProps
      , type
      , ...other
    } = this.props;

    let InputComponent = inputComponent;

    let inputProps = other as any;

    if(select) {
      InputComponent = NativeSelectInput;
    } else if (multiline) {
      inputProps = {
        maxLength
        // , placeholder: ''
        , ...inputProps
      };
      if (rows && !rowsMax) {
        InputComponent = 'textarea';
      } else {
        inputProps = {
          rows
          , rowsMax
          , shadowClassName: 'fl-field-shadow'
          , ...inputProps
        };
        InputComponent = TextareaAutosize;
      }
    } else {
      inputProps = {
        maxLength
        // , placeholder: ''
        , type
        , ...inputProps
      };
    }

    const flGroupClassName = classNames({
      'fl-group-multiline': multiline
      , 'fl-group-select': select
      , [className]: !!className
    });

    return (
      <FlGroup
        className={flGroupClassName}
        ref={this._wrapperRef}
      >
        {label ? (
          <label className="fl-label">
            {label}
          </label>
        ) : null}
        <div className="fl-control">
          <InputComponent
            className="fl-field"
            {...inputProps}
            ref={inputRef}
          />
          {addon}
        </div>
        {helperText && (
          <FormHelperText>
            {helperText}
          </FormHelperText>
        )}
      </FlGroup>
    );
  }

  _onFocus = () => _toggleFocused(this.container, true);

  _onBlur = () => _toggleFocused(this.container, false);

  _updateClasses = () => {
    this._checkDisabled();
    this._checkError();
    this._checkFilled();
    this._checkFocused();
  };

  _checkDisabled = () => {
    _toggleDisabled(this.container, this.input.disabled);
  };

  _checkError = () => {
    _toggleError(this.container, this.props.error);
  };

  _checkFocused = () => {
    // console.log();
    _toggleFocused(this.container, Boolean(this.container.querySelector(':focus')));
  };

  _isFilled() {
    const {value = '', placeholder = ''} = this.input || this.props;
    return this.props.select || ( value && value.length > 0 ) || placeholder.trim() !== '';
  }

  _checkFilled = () => {
    _toggleFilled(this.container, this._isFilled());
  };
}

export default forwardRef(function TextField(props: TextFieldProps, ref: React.RefObject<any>) {
  return (
    <TextFieldClass inputRef={ref || createRef()} {...props}/>
  );
});
