import React, { forwardRef, useState, useRef, useCallback, HTMLAttributes } from 'react';
import { useForkRef } from '../../utils/react-helpers';

const getStyleValue = (computedStyle, property) => parseInt(computedStyle[property], 10) || 0;

const useEnhancedEffect = typeof window !== 'undefined' ? React.useLayoutEffect : React.useEffect;

export interface TextareaAutosizeProps extends HTMLAttributes<HTMLTextAreaElement>{
  /**
   * Number of rows to display when multiline option is set to true.
   */
  rows?: string | number;
  /**
   * Maximum number of rows to display when multiline option is set to true.
   */
  rowsMax?: string | number;
  shadowClassName?: string;
  value?: string;
}

interface TextareaAutosizeState {
  innerHeight?: number;
  outerHeight?: number;
  outerHeightStyle?: number;
}

/**
 * Extracted from MUI almost as is
 * window resize handler commented
 * added check for change og real input height
 * @type {React.ComponentType<{} & React.ClassAttributes<unknown>>}
 */
const TextareaAutosize = forwardRef(function TextareaAutosize(props: TextareaAutosizeProps, ref) {
  const {
    onChange
    , rows = 1
    , rowsMax = 4
    , style
    , value
    , shadowClassName
    , className
    , ...other
  } = props;

  const { current: isControlled } = useRef(value != null);
  const inputRef = useRef(null);
  const handleRef = useForkRef(ref, inputRef);
  const shadowRef = useRef(null);
  const [state, setState] = useState<TextareaAutosizeState>({});

  const syncHeight = useCallback(() => {
    const input = inputRef.current;
    const computedStyle = window.getComputedStyle(input);

    const inputShallow = shadowRef.current;
    inputShallow.style.width = computedStyle.width;
    inputShallow.value = input.value || props.placeholder || 'x';

    const boxSizing = computedStyle['box-sizing'];
    const padding =
      getStyleValue(computedStyle, 'padding-bottom') + getStyleValue(computedStyle, 'padding-top');
    const border =
      getStyleValue(computedStyle, 'border-bottom-width') +
      getStyleValue(computedStyle, 'border-top-width');

    // The height of the inner content
    const innerHeight = inputShallow.scrollHeight - padding;

    // Measure height of a textarea with a single row
    inputShallow.value = 'x';
    const singleRowHeight = inputShallow.scrollHeight - padding;

    // The height of the outer content
    let outerHeight = innerHeight;

    if (rows != null) {
      outerHeight = Math.max(Number(rows) * singleRowHeight, outerHeight);
    }
    if (rowsMax != null) {
      outerHeight = Math.min(Number(rowsMax) * singleRowHeight, outerHeight);
    }
    outerHeight = Math.max(outerHeight, singleRowHeight);

    // Take the box sizing into account for applying this value as a style.
    const outerHeightStyle = outerHeight + (boxSizing === 'border-box' ? padding + border : 0);

    setState(prevState => {
      // Need a large enough different to update the height.
      // This prevents infinite rendering loop.

      if (
        outerHeightStyle > 0 && (
          Math.abs((prevState.outerHeightStyle || 0) - outerHeightStyle) > 1 ||
          Math.abs((prevState.innerHeight || 0) - innerHeight) > 1
        )
      ) {
        return {
          innerHeight,
          outerHeight,
          outerHeightStyle,
        };
      }

      return prevState;
    });
  }, [setState, rows, rowsMax, props.placeholder]);

  useEnhancedEffect(() => {
    syncHeight();
  });

  const handleChange = event => {
    if (!isControlled) {
      syncHeight();
    }

    if (onChange) {
      onChange(event);
    }
  };

  return (
    <React.Fragment>
      <textarea
        className={className}
        value={value}
        onChange={handleChange}
        ref={handleRef}
        // Apply the rows prop to get a "correct" first SSR paint
        rows={Number(rows || 1)}
        style={{
          height: state.outerHeightStyle
          // Need a large enough different to allow scrolling.
          // This prevents infinite rendering loop.
          , overflow: Math.abs(state.outerHeight - state.innerHeight) <= 1 ? 'hidden' : null
          , ...style
        }}
        {...other}
      />
      <textarea
        className={shadowClassName}
        aria-hidden
        readOnly
        ref={shadowRef}
        tabIndex={-1}
        style={style}
      />
    </React.Fragment>
  );
});

export default TextareaAutosize;
