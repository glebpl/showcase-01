import React from 'react';
import {
  ToastContainer as ToastifyToastContainer,
  toast as toastifyToast,
  Slide,
  Toast,
  ToastContent, ToastOptions, ToastId
} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const _defaults = {
  hideProgressBar: true
  , bodyClassName: 'toast-body'
  , autoClose: 4000
  , transition: Slide
};

const _withType = (fn, type) => (content, options = {}) => (
  fn(content, {...options, className: `toast toast-${type}`})
);

const _withDefaults = (fn, def = _defaults) => (content, options = {}) => (
  fn(content, {...def, ...options})
);

const _withShortAutoClose = (fn, tmt = 1500) => (content, options = {}) => (
  fn(content, {...options, autoClose: tmt})
);

const _withLongAutoClose = (fn, tmt = 8000) => (content, options = {}) => (
  fn(content, {...options, autoClose: tmt})
);


const {
  warn: _warn
  , success: _success
  , error: _error
} = toastifyToast;

type ToastMethod = (content: ToastContent, options?: ToastOptions) => ToastId

export interface ExtendedToast extends Toast {
  successShort: ToastMethod;
  successLong: ToastMethod;
  warnLong: ToastMethod;
  errorShort: ToastMethod;
  errorLong: ToastMethod;
}

export const toast = toastifyToast as ExtendedToast;

toast.success = _withType(_withDefaults(_success), 'success');
toast.successShort = _withShortAutoClose(toast.success);
toast.successLong = _withLongAutoClose(toast.success);
toast.warn = _withType(_withDefaults(_warn), 'warning');
toast.warnLong = _withLongAutoClose(toast.warn);
toast.error = _withType(_withDefaults(_error), 'error');
toast.errorShort = _withShortAutoClose(toast.error);
toast.errorLong = _withLongAutoClose(toast.error);

// default 'X' for close may be replaces using closeButton option

export const ToastContainer = (): React.ReactElement<ToastifyToastContainer> => (
  <ToastifyToastContainer
    className="toast-container"
    toastClassName="toast"
    bodyClassName="toast-body"
  />
);
