import { Component } from 'react';
import { findDOMNode, createPortal } from 'react-dom';
import PropTypes from 'prop-types';
import { domOwnerDocument } from '../../utils/dom';

const getContainer = (container, defaultContainer) => {
  container = typeof container === 'function' ? container() : container;
  return findDOMNode(container) || defaultContainer;
};

const getOwnerDocument = element => domOwnerDocument(findDOMNode(element));

/**
 * Taken from https://material-ui.com and refactored
 * Portals provide a first-class way to render children into a DOM node
 * that exists outside the DOM hierarchy of the parent component.
 */
export default class Portal extends Component {
  static propTypes = {
    /**
     * The children to render into the `container`.
     */
    children: PropTypes.node.isRequired
    /**
     * A node, component instance, or function that returns either.
     * The `container` will have the portal children appended to it.
     * By default, it uses the body of the top-level document object,
     * so it's simply `document.body` most of the time.
     */
    , container: PropTypes.oneOfType([PropTypes.object, PropTypes.func])
    /**
     * Disable the portal behavior.
     * The children stay within it's parent DOM hierarchy.
     */
    , disablePortal: PropTypes.bool
    /**
     * Callback fired once the children has been mounted into the `container`.
     */
    , onRendered: PropTypes.func
  };

  static defaultProps = {
    disablePortal: false
  };

  constructor(props) {
    super(props);
    this._el = null;
  }

  get el() {
    return this._el;
  }

  componentDidMount() {
    this.setElement(this.props.container);

    // Only rerender if needed
    if (!this.props.disablePortal) {
      this.forceUpdate(this.props.onRendered);
    }
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.container !== this.props.container ||
            prevProps.disablePortal !== this.props.disablePortal
    ) {
      this.setElement(this.props.container);

      // Only rerender if needed
      if (!this.props.disablePortal) {
        this.forceUpdate(this.props.onRendered);
      }
    }
  }

  componentWillUnmount() {
    this._el = null;
  }

  setElement(container) {
    if (this.props.disablePortal) {
      this._el = findDOMNode(this).parentElement;
      return;
    }

    this._el = getContainer(container, getOwnerDocument(this).body);
  }

  render() {
    const {children, disablePortal} = this.props;

    if (disablePortal) {
      return children;
    }

    return this._el ? createPortal(children, this._el) : null;
  }
}
