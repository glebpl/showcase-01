import React, { useEffect, useRef } from 'react';
import h337 from 'heatmap.js';
import { domBind, getRect } from '../utils/dom';

export interface SampleHeatmapProps {}

/**
 * Draws heatmap of mousemove events
 */
const SampleHeatmap: React.FC<SampleHeatmapProps> = () => {

  const hmRef = useRef(null);
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    hmRef.current = h337.create({
      container: ref.current
      , radius: 60
    });

    domBind(ref.current, 'click', (e: MouseEvent) => {
      const {left, top} = getRect(ref.current);
      hmRef.current.addData({
        x: e.clientX - left
        , y: e.clientY - top
        , value: 1.25
      });
    });
  }, []);

  return (
    <div ref={ref} className="heatmap-container">
      <div className="heatmap-hint">Click me</div>
    </div>
  );
};

export default SampleHeatmap;
