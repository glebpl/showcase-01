import React, { useMemo, useState } from 'react';
import { BtnIcon } from '../style/buttons';
import { IconNotVisible, IconVisible } from '../style/icons';
import TextField, { TextFieldProps } from '../style/TextField';

export interface PasswordFieldProps extends TextFieldProps {
  show: boolean;
}

const PasswordField: React.FC<PasswordFieldProps> = props => {
  const {show: propShow, ...rest} = props;

  const [show, setShow] = useState(propShow);

  const addon = useMemo(() => (
    <BtnIcon onClick={() => setShow(!show)}>
      {!show ? <IconNotVisible/> : <IconVisible/>}
    </BtnIcon>
  ), [show]);

  return (
    <TextField
      className="field-block field-password"
      type={show ? 'text' : 'password'}
      name="password"
      autoComplete="current-password"
      addon={addon}
      {...rest}
    />
  );
};

export default PasswordField;
