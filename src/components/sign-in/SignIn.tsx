import React, { FormEvent, useRef, useCallback, useMemo, useReducer, useContext } from 'react';
import i18n from 'i18next';
import { useTranslation } from 'react-i18next';
import { connect, ConnectedProps } from 'react-redux';
import { signIn } from '../../store/actions';
import Card from '../style/Card';
import Form from '../style/Form';
import { BtnBlockPrimary } from '../style/buttons';
import { LanguageCode } from '../../i18n';
import FormGroup from '../style/FormGroup';
import DropdownSelect from '../style/dropdowns/DropdownSelect';
import TextField from '../style/TextField';
import PasswordField from './PasswordField';
import AppContext from '../AppContext';

const reducer = (state, {type, data = {}}) => {
  switch(type) {
    case 'update':
      return {
        ...state
        , ...data
      };
    default:
      return state;
  }
};

const update = data => ({
  type: 'update'
  , data
});

const mapDispatchToProps = dispatch => ({
  signIn: (login: string, password: string) => dispatch(signIn({name: login, password}))
});

const storeConnector = connect(null, mapDispatchToProps);

type SignInProps = ConnectedProps<typeof storeConnector>

const SignIn: React.FC<SignInProps> = props => {
  const {t} = useTranslation();
  const {signIn} = props;
  const {secure} = useContext(AppContext);

  const langs = useMemo(() => Object.values(LanguageCode).map(langKey => ({value: langKey})), []);

  const [state, dispatch] = useReducer(reducer, {
    login: ''
    , loginError: false
    , password: ''
    , passwordError: false
    , showPassword: false
    , processingStarted: false
  });
  const {login, loginError, password, passwordError} = state;

  const procStartedRef = useRef(false);

  const handleChange = useCallback(e => {
    const {name, value} = e.target;
    dispatch(update({
      [name]: value
      , [`${name}Error`]: procStartedRef.current && !value.trim()
    }));
  }, []);

  const formatLanguage = useCallback((key: LanguageCode) => t(key), [t]);

  const handleChangeLanguage = useCallback(e => {
    const {value} = e.target;
    i18n.changeLanguage(value);
  }, []);

  const handleSubmit = useCallback((e: FormEvent) => {
    e.preventDefault();

    procStartedRef.current = true;

    if(login && password) {
      dispatch(update({
        loginError: false
        , passwordError: false
      }));
      signIn(login, password);
    } else {
      dispatch(update({
        loginError: !login
        , passwordError: !password
      }));
    }
  }, [login, password]);

  return (
    <Card className="card-public card-login">
      <h1 className="card-title">{t('authorization')}</h1>
      {secure ? null : (
        <div className="alert alert-warning">
          {t('httpsWarning')}
        </div>
      )}
      <div className="card-content">
        <Form onSubmit={handleSubmit}>
          <FormGroup>
            <TextField
              className="field-block field-login"
              name="login"
              autoFocus={true}
              label={t('username')}
              value={login}
              autoComplete="username"
              onChange={handleChange}
              error={loginError}
              maxLength={25}
            />
          </FormGroup>
          <FormGroup>
            <PasswordField
              label={t('password')}
              value={password}
              onChange={handleChange}
              error={passwordError}
              show={state.showPassword}
              maxLength={25}
            />
          </FormGroup>
          <FormGroup className="form-group-lang">
            <DropdownSelect
              name="lang"
              value={i18n.language}
              onChange={handleChangeLanguage}
              menuItems={langs}
              formatValue={formatLanguage}
              label={t('language')}
            />
          </FormGroup>
          <BtnBlockPrimary type="submit">{t('signIn')}</BtnBlockPrimary>
        </Form>
      </div>
    </Card>
  );
};

export default storeConnector(SignIn);
