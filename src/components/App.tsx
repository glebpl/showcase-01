import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import { ToastContainer } from './style/toasts';
import { isSecureLocation } from '../utils/transport';
import AppContext from './AppContext';
import { privatePath } from '../routes';
import PrivateApp from './PrivateApp';
import PublicApp from './PublicApp';
import { initialize } from '../store/actions';
import { StoreState } from '../store/types';
import { getAuthStatus } from '../store/selectors';
import { AuthStatus } from '../constants';

const mapStateToProps = (state: StoreState) => ({
  authStatus: getAuthStatus(state)
});

const mapDispatchToProps = dispatch => ({
  initialize: () => dispatch(initialize())
});

const storeConnector = connect(mapStateToProps, mapDispatchToProps);

type AppProps = ConnectedProps<typeof storeConnector> & {
  location: Location;
}

const App: React.FC<AppProps> = props => {
  const {authStatus, location, initialize} = props;

  useEffect(() => {
    // will check if we have stored credential
    initialize();
  }, []);

  return (
    <AppContext.Provider value={{
      secure: isSecureLocation(location)
    }}>
      <div className="app">
        {authStatus !== AuthStatus.UNKNOWN  ? (
          <Switch>
            <Route path={privatePath()}>
              <PrivateApp />
            </Route>
            <Route path="/">
              <PublicApp />
            </Route>
          </Switch>
        ) : null }
        <ToastContainer />
      </div>
    </AppContext.Provider>
  );
};

export default storeConnector(App);
