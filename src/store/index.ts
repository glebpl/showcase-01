import { createStore, combineReducers, applyMiddleware, Reducer } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { auth } from './reducers/auth';

const createReducer = (asyncReducers = {}): Reducer => (
  combineReducers({
    auth
    , ...asyncReducers
  })
);

const logger = createLogger({
  level: process.env.NODE_ENV === 'development' ? 'log' : 'error'
});

const middlewares = [thunkMiddleware, logger];

export function configureStore(initialState = {}) {
  return createStore(createReducer(), initialState, applyMiddleware(...middlewares));
}
