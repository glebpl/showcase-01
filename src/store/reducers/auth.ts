import { AuthStatus } from '../../constants';
import { Reducer } from 'redux';
import { ActionType, AuthReducerKey, AuthReducerState, StoreAction } from '../types';

const initialState: AuthReducerState = {
  [AuthReducerKey.USER_NAME]: ''
  , [AuthReducerKey.STATUS]: AuthStatus.UNKNOWN
  , [AuthReducerKey.ERROR]: null
};

export const auth: Reducer<AuthReducerState, StoreAction> = (state = initialState, {type, data}) => {
  switch (type) {
    case ActionType.AUTH_NO_CRED:
      return {
        ...state
        , [AuthReducerKey.STATUS]: AuthStatus.NONE
        , [AuthReducerKey.USER_NAME]: ''
        , [AuthReducerKey.ERROR]: null
      };
    case ActionType.AUTH_SUCCESS:
      return {
        ...state
        , [AuthReducerKey.STATUS]: AuthStatus.CONFIRMED
        , [AuthReducerKey.USER_NAME]: data[AuthReducerKey.USER_NAME]
        , [AuthReducerKey.ERROR]: null
      };
    case ActionType.AUTH_FAIL:
      return {
        ...state
        , [AuthReducerKey.STATUS]: AuthStatus.NONE
        , [AuthReducerKey.USER_NAME]: ''
        , [AuthReducerKey.ERROR]: data[AuthReducerKey.ERROR]
      };
    case ActionType.UNAUTH_SUCCESS:
      return {
        ...state
        , [AuthReducerKey.STATUS]: AuthStatus.NONE
        , [AuthReducerKey.ERROR]: null
      };
    default:
      return state;
  }
};
