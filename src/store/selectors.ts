import { AuthReducerKey, ReducerKey, StoreState } from './types';
import { AuthStatus } from '../constants';

export const getAuthStatus = (state: StoreState): AuthStatus => state[ReducerKey.AUTH][AuthReducerKey.STATUS];
export const getAuthError = (state: StoreState): number => state[ReducerKey.AUTH][AuthReducerKey.ERROR];
export const getUserName = (state: StoreState): string => state[ReducerKey.AUTH][AuthReducerKey.USER_NAME];
