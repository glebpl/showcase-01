import { AuthStatus, ErrorCode } from '../constants';

export enum ActionType {
  AUTH_NO_CRED = 'noCred'
  , AUTH_SUCCESS = 'authSuccess'
  , AUTH_FAIL = 'authFail'
  , UNAUTH_SUCCESS = 'unauthSuccess'
}

export enum ReducerKey {
  AUTH = 'auth'
}

export enum AuthReducerKey {
  STATUS = 'authStatus'
  , USER_NAME = 'userName'
  , ERROR = 'error'
}

export type AuthReducerState<K extends keyof any = AuthReducerKey> = {
  [P in K]: (
    P extends AuthReducerKey.USER_NAME ? string :
    P extends AuthReducerKey.STATUS ? AuthStatus :
    P extends AuthReducerKey.ERROR ? ErrorCode :
    never
  );
};

export type AuthReducerData<A> =
  A extends ActionType.AUTH_SUCCESS ? (
    Pick<AuthReducerState, AuthReducerKey.USER_NAME>
  ) :
  A extends ActionType.AUTH_FAIL ? (
    Pick<AuthReducerState, AuthReducerKey.ERROR>
  ) :
  never

export type StoreAction<A = ActionType> = {
  type: A;
  data?: AuthReducerData<A>;
};

export type StoreState<K extends keyof any = ReducerKey> = {
  [P in K]: (
    P extends ReducerKey.AUTH ? AuthReducerState :
    never
  );
};
