import { PasswordCredential, ThunkSimpleAction } from '../types';
import { ActionType, AuthReducerKey, StoreAction } from './types';
import fakeAuthProvider from '../data/AuthProvider';

/**
 * Thunk action dispatched on application start
 * Checks if we have stored credential
 */
export const initialize = (): ThunkSimpleAction<void> => async dispatch => {
  const cred = fakeAuthProvider.getCredentialFromStorage();
  if(cred) {
    try {
      await fakeAuthProvider.getTokenByCredential(cred);
      dispatch(authSuccess(cred));
    } catch (ex) {
      dispatch(authNoCredential());
    }
  } else {
    dispatch(authNoCredential());
  }
};

/**
 * Thunk action to be used for authorization
 * @param cred
 */
export const signIn = (cred: PasswordCredential) => async dispatch => {
  try {
    await fakeAuthProvider.getTokenByCredential(cred);
    fakeAuthProvider.storeCredential(cred);
    dispatch(authSuccess(cred));
  } catch (err) {
    dispatch(authFail(err));
  }
};

/**
 * Thunk action to be used to cancel authorization
 */
export const signOut = () => async dispatch => {
  try {
    await fakeAuthProvider.signOut();
    dispatch(unauthSuccess());
  } catch (err) {
    // console.log(err)
  }
};

/**
 * Helper action to detect unknown/none state of authorization
 */
export const authNoCredential = (): StoreAction<ActionType.AUTH_NO_CRED> => ({
   type: ActionType.AUTH_NO_CRED
});

export const authSuccess = ({name}: PasswordCredential): StoreAction<ActionType.AUTH_SUCCESS> => ({
  type: ActionType.AUTH_SUCCESS
  , data: {
    [AuthReducerKey.USER_NAME]: name
  }
});

export const authFail = (error: number): StoreAction<ActionType.AUTH_FAIL> => ({
  type: ActionType.AUTH_FAIL
  , data: {
    [AuthReducerKey.ERROR]: error
  }
});

export const unauthSuccess = (): StoreAction<ActionType.UNAUTH_SUCCESS> => ({
  type: ActionType.UNAUTH_SUCCESS
});
