import { PasswordCredential } from '../types';
import { getCredentialStorage, PublicStorage } from '../utils/storage';

export interface AuthProvider {
  storage: PublicStorage;
  getCredentialFromStorage(): PasswordCredential | null;
  getTokenByCredential(cred: PasswordCredential): Promise<string>;
  storeCredential(cred: PasswordCredential): void;
  signOut(): Promise<void>;
  invalidateStoredCredential(): void;
}

const _key = 'm';

// some simple encryption
const key = 120220;
const cr = (s: string): string => [...s].map(char => String.fromCharCode(char.charCodeAt(0) ^ key)).join('');
const de = cr;

const fakeAuthProvider: AuthProvider = {
  storage: getCredentialStorage()

  , getCredentialFromStorage() {
    const storage = this.storage;
    const stored = storage.get(_key, '');
    if(stored) {
      try {
        return JSON.parse(de(stored)) as PasswordCredential;
      } catch (ex) {
        // this.clear();
        return null;
      }
    } else {
      return null;
    }
  }

  , storeCredential(cred) {
    this.storage.set(_key, cr(JSON.stringify(cred)));
  }

  , async getTokenByCredential() {
    let promise = new Promise<string>(resolve => {
      setTimeout(() => resolve('faketoken'), 100);
    });

    return await promise;
  }

  , async signOut() {
    return new Promise<void>(resolve => {
      setTimeout(() => {
        this.invalidateStoredCredential();
        resolve();
      }, 100);
    });
  }

  , invalidateStoredCredential() {
    return this.storage.remove(_key);
  }
};

export default fakeAuthProvider;
