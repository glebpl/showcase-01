export const URL_SIGN_IN = '/sign-in';
export const URL_PRIVATE_BASE = '/my';

export const privatePath = (path = '/'): string => `${URL_PRIVATE_BASE}${path === '/' ? '' : path}`;
export const privateUrl = (path: string): string => `#${privatePath(path)}`;
