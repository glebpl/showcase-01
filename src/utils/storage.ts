const parse = (v: null | string): any => {
  if(v !== null) {
    try {
      // numbers, booleans will be parsed
      return JSON.parse(v);
    } catch (e) {
      return v;
    }
  } else {
    return null;
  }
};

let _sessionStorage: PublicStorage = null;
let _credentialStorage: PublicStorage = null;

export class PublicStorage {
  _engine: Storage;

  constructor(engine: Storage) {
    this._engine = engine;
  }

  get engine(): Storage {
    return this._engine;
  }

  has(key: string): boolean {
    return key in this.engine;
  }

  get(key: string, def = null): any {
    return this.has(key) ? parse(this.engine.getItem(key)) : def;
  }

  set(key: string, value: any): void {
    // null has type 'object'
    if('object' === typeof value && value !== null) {
      value = JSON.stringify(value);
    }
    this.engine.setItem(key, value);
  }

  remove(...keys: string[]): void {
    keys.map(key => this.engine.removeItem(key));
  }

  clear(): void {
    this.engine.clear();
  }
}

export const getSessionStorage = (): PublicStorage => (
  _sessionStorage || ( _sessionStorage = new PublicStorage(window.sessionStorage))
);

/**
 * Here we use just not secure local storage
 * In real application navigator.credentials API may be used
 */
export const getCredentialStorage = (): PublicStorage => (
  _credentialStorage || ( _credentialStorage = new PublicStorage(window.localStorage))
);
