/**
 * Makes first letter uppercase
 * @param {string} s
 * @return {string}
 */
export const ucFirst = (s: any): string => {
  s = String(s);
  return s.substr(0, 1).toUpperCase() + s.substr(1);
};
