import { BooleanMap } from '../types';

/**
 * Makes className from array of names
 * Filters not empty ones
 * @param {String[]|Object} names
 */
export const classNames = (names: string | string[] | BooleanMap): string => {
  let result = '';
  if (Array.isArray(names)) {
    result = names.filter(n => n && n.trim().length > 0).join(' ');
  } else if ('object' === typeof names) {
    const arr = [];
    for (let n in names) {
      if (n && names[n]) {
        arr.push(n);
      }
    }
    result = classNames(arr);
  } else {
    result = names;
  }
  return result;
};

export const createClassToggle = (className: string) => (el: HTMLElement, on: boolean) => {
  el && el.classList[on ? 'add' : 'remove'](className);
};
