import { TransitionOptions, TransitionTimeout, BasicTransitionProps } from '../types';

const formatMs = (ms: number): string => `${Math.round(ms)}ms`;

export const domOwnerDocument = (el: HTMLElement | Document): Document => (el && el.ownerDocument) || document;

export const domOwnerWindow = (el: HTMLElement | Document, fallback = window): Window => {
  const doc = domOwnerDocument(el);
  return doc.defaultView || fallback;
};

export const getRect = (el: HTMLElement): DOMRect => el.getBoundingClientRect();

/**
 * Adds event handler
 * @param {HTMLElement} el
 * @param {string} eventName
 * @param {function} handler
 * @param {Object} options
 */
export const domBind = (
  el: HTMLElement
  , eventName: string
  , handler: EventListenerOrEventListenerObject
  , options = {}
): void => (
  el.addEventListener(eventName, handler, options)
);

/**
 * Extracts data from html 'data-' attribute
 * Also can accept other objects providing attributes property
 * @param {Object} el
 * @param {String} key
 * @return {null|String}
 */
export const domGetData = (el: HTMLElement, key: string): null | string => {
  return key in el.dataset ? el.dataset[key] : null;
};

/**
 * Helper to restart animation
 * Taken from mui
 * @param el
 */
export const domReflow = (el: HTMLElement): number => el.scrollTop;

/**
 * Creates css string for transition
 * @param props
 * @param options
 */
export const domCreateTransitions = (props: string | string[] = ['all'], options: TransitionOptions = {}): string => {
  const {
    duration: durationOption = 300
    , easing: easingOption = 'cubic-bezier(0.4, 0, 0.2, 1)'
    , delay = 0
  } = options;

  return (Array.isArray(props) ? props : [props])
    .map(
      animatedProp =>
        `${animatedProp} ${
          typeof durationOption === 'string' ? durationOption : formatMs(durationOption)
        } ${easingOption} ${typeof delay === 'string' ? delay : formatMs(delay)}`,
    )
    .join(',');
};

/**
 * Element transition delay and duration
 * @param props
 * @param options
 * @return {{duration: (Number.timeout|*), delay: *}}
 */
export const domGetTransitionProps = (props: {
  timeout?: TransitionTimeout;
  style?: {transitionDuration?: string | number; transitionDelay: string | number};
}, options: {mode: string}): BasicTransitionProps => {
  const { timeout, style = {transitionDelay: 0} } = props;

  return {
    duration:
      style.transitionDuration || typeof timeout === 'number' ? timeout : timeout[options.mode]
    , delay: style.transitionDelay
  };
};

/**
 * Taken from material-ui
 * @param height
 * @return {number}
 */
export const domGetAutoHeightDuration = (height: number): number => {
  if (!height) {
    return 0;
  }

  const constant = height / 36;

  // https://www.wolframalpha.com/input/?i=(4+%2B+15+*+(x+%2F+36+)+**+0.25+%2B+(x+%2F+36)+%2F+5)+*+10
  return Math.round((4 + 15 * constant ** 0.25 + constant / 5) * 10);
};

export const domIsWindow = (el: any): boolean => el === el.window;

export const domIsBody = (el: HTMLElement): boolean => el && el.tagName.toLowerCase() === 'body';

// Do we have a vertical scroll bar?
export const domIsOverflowing = (container: HTMLElement): boolean => {
  const doc = domOwnerDocument(container);
  const win = domOwnerWindow(doc);

  if (!domIsWindow(doc) && !domIsBody(container)) {
    return container.scrollHeight > container.clientHeight;
  }

  // Takes in account potential non zero margin on the body.
  const style = win.getComputedStyle(doc.body);
  const marginLeft = parseInt(style.getPropertyValue('margin-left'), 10);
  const marginRight = parseInt(style.getPropertyValue('margin-right'), 10);

  return marginLeft + doc.body.clientWidth + marginRight < win.innerWidth;
};

interface EventLike {
  target: {name: string; value: any};
  reactEvent: any;
}

export const likeEvent = (name: string, value: any, reactEvent = null): EventLike => ({
  target: {
    name, value
  }
  , reactEvent
});
