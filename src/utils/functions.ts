import { Timeout } from '../types';

/**
 * If passed argument is a function it will be called, else same value returned
 * @param v
 */
export const result = (v: any): any => 'function' === typeof v ? v() : v;

/**
 * Safe chained function
 *
 * Will only create a new function if needed,
 * otherwise will pass back existing functions or null.
 *
 * @param {function} funcs to chain
 * @returns {function|null}
 */
export const createChainedFunction = (...funcs: Function[]): Function => {
  return funcs.reduce(
    (acc, func) => {
      if ('function' !== typeof func) {
        return acc;
      }

      return function chainedFunction(this: any, ...args: any[]) {
        acc.apply(this, args);
        func.apply(this, args);
      };
    },
    () => {}
  );
};

/**
 * Taken from MUI
 * Corresponds to 10 frames at 60 Hz
 * A few bytes payload overhead when lodash/debounce is ~3 kB and debounce ~300 B
 * @param func
 * @param wait
 * @return {debounced}
 */
export const debounce = (func: Function, wait = 166): Function => {
  let timeout: Timeout;

  function debounced(this: any, ...args: any[]): void {
    // eslint-disable-next-line consistent-this
    const that: Function = this;
    const later = (): void => {
      func.apply(that, args);
    };
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  }

  debounced.clear = () => {
    clearTimeout(timeout);
  };

  return debounced;
};
