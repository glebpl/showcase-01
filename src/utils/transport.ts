export const isSecureLocation = ({protocol = ''}): boolean => protocol === 'https:';
